#include<iostream>
#include "sphere.h"
#include "hitable_list.h"
#include "float.h"
#include "camera.h"
#include "material.h"
#include "util.h"
//global variables declaration
FILE *gpImageFile = NULL;

//global functions
vec3 color(ray& r, hitable *world, int depth)
{
	//function prototype declaration
	vec3 random_in_unit_sphere(void);

	//local variables
	hit_record rec;

	//code
	//t_min = 0.001 because some of the reflected rays hit the object they are reflecting off
	//of not at exactly t=0, but instead at t=-0.0000001 or t=0.0000001 or whatever floating point
	//approximations the sphere intersectors gives us; so we need to ignore hits very near to zero;
	//this gets rids of the shadow acne problem
	if(world->hit(r, 0.001, FLT_MAX, rec))
	{
		ray scattered;
		vec3 attenuated;
		if(depth<50 && rec.mat_ptr->scatter(r, rec, attenuated, scattered))
		{
			return attenuated*color(scattered, world, depth+1);
		}else
		{
			return vec3(0,0,0);
		}

	}else
	{
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0f);
		return (1.0f - t) * vec3(1.0f, 1.0f, 1.0f) + t*vec3(0.5f, 0.7f, 1.0f);
	}
}

int main()
{
int nx = 200; //columns
int ny = 100; //rows
int ns = 100; //sample points
//open file
gpImageFile = fopen("finalimage.ppm", "w");

//std::cout<< "P3\n" << nx <<" " << ny <<"\n255\n";
fprintf(gpImageFile, "P3\n%d %d\n255\n",nx, ny);

hitable *list[5];
list[0] = new sphere(vec3(0,0,-1), 0.5, new lambertian(vec3(0.1, 0.2, 0.5)));
list[1] = new sphere(vec3(0, -100.5, -1), 100, new lambertian(vec3(0.8, 0.8, 0.0)));
list[2] = new sphere(vec3(1,0,-1), 0.5, new metal(vec3(0.8, 0.6, 0.2), 0.3));
list[3] = new sphere(vec3(-1,0,-1), //center
		0.5, //radius
		new dielectric(1.5)); //1.5 is dielectric index

/* An interesting and easy trick with a dielectric sphere is to note that if you use a negative radius,
 * the geometry is unaffected but the surface normal points inwards, so it can be used as a bubble
 * to make a hollow glass sphere
 * */
list[4] = new sphere(vec3(-1,0,-1), //center
		-0.45, //radius
		new dielectric(1.5)); //1.5 is dielectric index
hitable *world = new hitable_list(list, 5);

camera cam;
for(int j=ny-1; j>=0; j--)
{
	for(int i=0; i<nx; i++)
	{
		float u = float(i) / float(nx);
		float v = float(j) / float(ny);
		vec3 col(0,0,0);
		for(int s=0; s<ns; s++)
		{
			float u = float(i + fRand()) / float(nx);
			float v = float(j + fRand()) / float(ny);
			ray r = cam.get_ray(u, v);
			col += color(r, world, 0);
		}
		col /= float(ns);

		//making the image gamma 2 corrected : means raising the color to 1/gamma i.e. 1/2
		col = vec3(sqrt(col[0]),sqrt(col[1]), sqrt(col[2]));
		int ir = int(255.99 * col[0]);
		int ig = int(255.99 * col[1]);
		int ib = int(255.99 * col[2]);
		//std::cout<< ir << " " << ig << " " << ib <<"\n";
		fprintf(gpImageFile, "%d %d %d\n",ir, ig, ib);
	}
}

//close the file
fclose(gpImageFile);
}
