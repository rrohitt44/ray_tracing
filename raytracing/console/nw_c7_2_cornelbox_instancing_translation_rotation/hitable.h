#ifndef HITABLEH
#define HITABLEH

#include "ray.h"
#include "aabb.h"
//material will tell us how rays interact with the surface
class material;

/*
 * get_sphere_uv: For Image Texture Mapping
 *
 * Spherical coordinates for unit radius spheres is
 * x = cos(phi) cos(theta)
 * y = sin(phi) cos(theta)
 * z = sin(theta)
 *
 * where
 * theta is the angle down from pole
 * and phi is the angle around the axis through the poles
 * */
void get_sphere_uv(const vec3& p, float& u, float& v)
{
float phi = atan2(p.z(), p.x());
float theta = asin(p.y());

//u and v are the text coordinates
u = 1 - (phi + M_PI) / (2 * M_PI);
v = (theta + M_PI/2) / M_PI;
}

struct hit_record
{
	float t;
	float u; //tex coord
	float v; //tex coord
	vec3 p;
	vec3 normal;

	//when a ray hits the surface e.g. sphere, this material pointer will be set to point at the
	//material pointer the sphere was given when it was set up in main() when we start up
	material *mat_ptr;
};

class hitable
{
public:
	//hit only counts if t_min<t<t_max
	virtual bool hit(ray& r, //ray which is going to hit the object
			float t_min,//hit minumum interval
			float t_max,//hit maximum interval
			hit_record& rec) const = 0;

	virtual bool bounding_box(float t0, float t1, aabb& box) const = 0;
};


/*
 * walls are facing wrong way so we need outward facing normals
 * this hitable holds another hitable but reverses the normals
 * */
class flip_normals : public hitable
{
public:
	//variable
	hitable *ptr;

	//constructors
	flip_normals(hitable *p) : ptr(p){}

	//overriden methods
	virtual bool hit(ray& r, float t_min, float t_max, hit_record& rec)const
	{
		if(ptr->hit(r, t_min, t_max, rec))
		{
			rec.normal = -rec.normal;
			return true;
		}else
		{
			return false;
		}
	}

	virtual bool bounding_box(float t0, float t1, aabb& box) const
	{
		return ptr->bounding_box(t0, t1, box);
	}
};
#endif
