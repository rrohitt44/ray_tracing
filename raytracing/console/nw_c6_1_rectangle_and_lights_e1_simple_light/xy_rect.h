#ifndef XYRECTH
#define XYRECTH

#include "hitable.h"

class xy_rect : public hitable
{
public:
	//variables
	material * mp;
	float x0, x1, y0, y1, k;

	//constructors
	xy_rect(){}
	xy_rect(float px0, float px1, float py0, float py1, float pk, material *pmat) :
		x0(px0), x1(px1), y0(py0), y1(py1), k(pk), mp(pmat){};


	//overriden methods
	//hit only counts if t_min<t<t_max
		virtual bool hit(ray& r, //ray which is going to hit the object
				float t_min,//hit minumum interval
				float t_max,//hit maximum interval
				hit_record& rec) const;

		virtual bool bounding_box(float t0, float t1, aabb& box) const
		{
			box = aabb(vec3(x0,y0,k-0.0001), vec3(x1,y1,k+0.0001));
			return true;
		}
};


bool xy_rect::hit(ray& r, //ray which is going to hit the object
				float t0,//hit minumum interval
				float t1,//hit maximum interval
				hit_record& rec) const
{
	float t = (k-r.origin().z()) / r.direction().z();

	if(t<t0 || t>t1)
	{
		return false;
	}

	float x = r.origin().x() + t*r.direction().x();
	float y = r.origin().y() + t*r.direction().y();

	if(x<x0 || x>x1 || y<y0 || y>y1)
	{
		return false;
	}

	rec.u = (x-x0) / (x1-x0);
	rec.v = (y-y0) / (y1-y0);
	rec.t = t;
	rec.mat_ptr = mp;
	rec.p = r.point_at_parameter(t);
	rec.normal = vec3(0,0,1);
	return true;
}


#endif
