#ifndef TEXTUREH
#define TEXTUREH

#include "perlin.h"

/*
 * function that makes the colors on a surface procedural
 * This can be systhesis code or image look up or both
 * */
class texture
{
public:
	virtual vec3 value(float u, float v, const vec3& p) const = 0;
};

class constant_texture : public texture
{
public:
	//varialbes
	vec3 color;

	//constructors
	constant_texture(){}
	constant_texture(vec3 c): color(c){}

	//methods
	virtual vec3 value(float u, float v, const vec3& p) const
	{
		return color;
	}
};

/*
 * We can create a checker texture by noting that the sign of a sine and cosine just alternates
 * in a regular way and if we multiply trig functions in all three dimensions, the sign of that
 * product forms a 3D checker pattern
 * */
class checker_texture : public texture
{
//scope
public:
	//variables
	texture *odd;
	texture *even;

	//constructor
	checker_texture(){}
	checker_texture(texture *t0, texture *t1) : odd(t0), even(t1){}

	//methods
	virtual vec3 value(float u, float v, const vec3& p) const
	{
		float sines = sin(10 * p.x()) * sin(10 * p.y()) * sin(10 * p.z());

		if(sines < 0)
		{
			return odd->value(u, v, p);
		}else
		{
			even->value(u, v, p);
		}
	}
};

/*
 * takes the floats between 0 and 1 and creates gray colors
 * */
class noise_texture : public texture
{
public:
	//variables
	perlin noise;

	//constructors
	noise_texture(){}

	//override method
	virtual vec3 value(float u, float v, const vec3& p) const
	{
		return vec3(1,1,1) * noise.noise(p);
	}
};
#endif
