#ifndef CAMERAH
#define CAMERAH

#include "ray.h"
#include "util.h"

class camera
{
public:
	vec3 lower_left_corner;
	vec3 origin;
	vec3 horizontal;
	vec3 verticle;
	vec3 u, v, w;
	float lens_radius;

	//constructors
	camera()
	{
		lower_left_corner = vec3(-2.0, -1.0, -1.0);
		horizontal = vec3(4.0, 0.0, 0.0);
		verticle = vec3(0.0, 2.0, 0.0);
		origin = vec3(0.0, 0.0, 0.0);
	}
	camera(
			vec3 lookfrom, //where the camera is
			vec3 lookat, //where camera is looking at
			vec3 vup, //viewing up vector
			float vfov, //top to bottom in degrees - try changing this and see the effect
			float aspect, //height to in distance(-z) ratio
			float aperture,
			float focus_dist
			)
	{
		lens_radius = aperture/2;
		float theta = vfov * M_PI / 180; //converting vfov from degrees to radian
		float half_height = tan(theta/2);
		float half_width = aspect * half_height;
		origin = lookfrom;
		w = unit_vector(lookfrom - lookat);
		u = unit_vector(cross(vup , w));
		v = cross(w, u);
		lower_left_corner = origin - half_width*focus_dist*u - half_height* focus_dist*v - focus_dist *w;
		horizontal = 2 * half_width * focus_dist * u;
		verticle = 2 * half_height * focus_dist * v;
	}

	ray get_ray(float s, float t)
	{
		//function prototype declaration
		vec3 random_in_unit_sphere();

		//code
		vec3 rd = lens_radius * random_in_unit_sphere();
		vec3 offset = u * rd.x() + v * rd.y();
		return ray(origin + offset, lower_left_corner+ s*horizontal + t*verticle - origin - offset);
	}

	ray get_ray(float s, float t, bool isSample)
	{
		return ray(origin, lower_left_corner+ s*horizontal + t*verticle - origin);
	}
};
#endif
