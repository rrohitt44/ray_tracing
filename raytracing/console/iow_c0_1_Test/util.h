#ifndef UTILH
#define UTILH

#include "stdlib.h"
#include "vec3.h"

double fRand()
{
	double fMin=0.0, fMax=1.0;
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

vec3 random_in_unit_sphere()
{
	vec3 p;
	do
	{
		p = 2.0 * vec3(fRand(), fRand(), fRand()) - vec3(1,1,1);
	}while(p.squared_length() >= 1.0);
	return p;
}
#endif
