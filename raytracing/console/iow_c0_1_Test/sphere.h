#ifndef SPHEREH
#define SPHEREH

#include "hitable.h"

class sphere : public hitable
{
public:
	vec3 center;
	float radius;
	material *mat_ptr;
	sphere(){}
	sphere(vec3 cen, float r) : center(cen), radius(r) {};
	sphere(vec3 cen, float r, material *m) : center(cen), radius(r), mat_ptr(m) {};
	virtual bool hit(ray& r, float tmin, float tmax, hit_record& rec) const;
};

bool sphere::hit(ray& r, float t_min, float t_max, hit_record& rec) const
{
vec3 oc = r.origin() - center;
float a = dot(r.direction(), r.direction());
float b = dot(oc, r.direction());
float c = dot(oc, oc) - radius * radius;
float discriminant = b*b - a*c;

if(discriminant>0)
{
	float temp = (-b -sqrt(discriminant))/a; //calculate where the ray hits sphere, it has two possible roots
	if(temp<t_max && temp>t_min)
	{
		rec.t = temp; //where ray hits on sphere
		rec.p = r.point_at_parameter(rec.t); //ray which hits the sphere at hitpoint t
		rec.normal = (rec.p - center)/radius;
		rec.mat_ptr = mat_ptr;
		return true;
	}
	temp = (-b +sqrt(discriminant))/a; //calculate where the ray hits sphere, it has two possible roots
	if(temp<t_max && temp>t_min)
		{
			rec.t = temp;
			rec.p = r.point_at_parameter(rec.t);
			rec.normal = (rec.p - center)/radius;
			rec.mat_ptr = mat_ptr;
			return true;
		}
}
return false;
}
#endif
