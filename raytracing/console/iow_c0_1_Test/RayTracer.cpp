//============================================================================
// Name        : RayTracer.cpp
// Author      : me
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include<iostream>
#include"sphere.h"
#include "camera.h"
#include "hitable_list.h"
#include "float.h"

using namespace std;

//global variables declaration
FILE *gpImageFile = NULL;

vec3 color(ray& r, hitable *world)
{
	hit_record rec;
	//0.001 to get rid of the shadow acne problem
	//problem: some of the reflected rays hits the object they are reflecting
	//off of not at exactly t=0 but instead at 0.0000001 or 0.00000001 or whatever
	//floating point approximation the sphere intersector gives us
	if(world->hit(r, 0.001, FLT_MAX, rec))
	{
		//rec.p + rec.normal is center of the sphere
		vec3 target = rec.p + rec.normal + random_in_unit_sphere();

		//we need diffuse material so below is not needed
		//return 0.5 * vec3(rec.normal.x()+1, rec.normal.y()+1, rec.normal.z()+1); //scale N to 0 to 1

		//for diffuse we are using below
		ray hpTos = ray(rec.p, target-rec.p);
		return 0.5 * color(hpTos, world);
	}
	else
	{
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t) * vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

int main()
{
int nx = 200; //columns
int ny = 100; //rows
int ns = 10; //sampling points
//open file

cout<<"tracer started-\n";
gpImageFile = fopen("finalimage.ppm", "w");

//std::cout<< "P3\n" << nx <<" " << ny <<"\n255\n";
fprintf(gpImageFile, "P3\n%d %d\n255\n",nx, ny);

hitable *list[3];
list[0] = new sphere(vec3(0,0,-1), 0.5);
list[1] = new sphere(vec3(0, -100.5, -1), 100);
list[2] = new sphere(vec3(1,0,-1), 0.3);
hitable *world = new hitable_list(list, 2);

camera cam;
for(int j=ny-1; j>=0; j--)
{
	for(int i=0; i<nx; i++)
	{
		vec3 col(0,0,0); //initial color is black
		for(int s=0; s<ns; s++)
		{
		float u = float(i + fRand())/float(nx);
		float v = float(j + fRand())/float(ny);

		ray r = cam.get_ray(u, v, true);
		col += color(r, world);
		}
		col /= float(ns); //averaging the color from samples

		//making the image gamma corrected; means 0 to 1 values have some transform before being stored as a byte
		//we use gamma 2 meaning raising the color to 1/gamma (1/2)
		col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
		int ir = int(255.99 * col[0]);
		int ig = int(255.99 * col[1]);
		int ib = int(255.99 * col[2]);
		//std::cout<< ir << " " << ig << " " << ib <<"\n";
		fprintf(gpImageFile, "%d %d %d\n",ir, ig, ib);
	}
}

//close the file
fclose(gpImageFile);
}

