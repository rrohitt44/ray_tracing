// Headers
#include<Windows.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine,
	int nCmdShow)
{
	//local variables
	WNDCLASSEX wndclass;
	TCHAR AppName[] = TEXT("Windows");
	HWND hwnd; //handle to window
	MSG msg;

	//code
	//initialize window class
	wndclass.cbSize = sizeof(WNDCLASSEX); //size in bytes of this structure
	wndclass.style = CS_HREDRAW | CS_VREDRAW; //class styles
	wndclass.cbClsExtra = 0; //number of extra bytes to allocate following the window-class structure
	wndclass.cbWndExtra = 0; //number of extra bytes to allocate following the window instance
	wndclass.lpszMenuName = NULL; //resource name of class menu
	wndclass.lpszClassName = AppName; //class name
	wndclass.lpfnWndProc = WndProc; //pointer to window procedure
	wndclass.hInstance = hInstance; //handle to the instance that contains the window procedure
	wndclass.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH); //handle to the class background brush
	wndclass.hIcon = LoadIcon(hInstance, TEXT("IDI_ICON1")); //handle to the class icon
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION); //handle to the small icon that is associated with the window class
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); //handle to the cursor class

	//register the window class
	RegisterClassEx(&wndclass);

	//create the window in memory
	hwnd = CreateWindow(
		AppName, //class name
		TEXT("First Window"), //window name
		WS_OVERLAPPEDWINDOW, //style
		CW_USEDEFAULT, //x
		CW_USEDEFAULT, //y
		CW_USEDEFAULT, //width
		CW_USEDEFAULT, //height
		NULL, //wnd parent
		NULL, //menu
		hInstance, //hInstance
		NULL //param
	);

	//error checking
	if (hwnd == NULL)
	{
		MessageBox(NULL, //hWnd
			TEXT("Window Not Created"), //text
			TEXT("Error..."), //caption
			0 //utype
		);

		exit(0);
	}

	ShowWindow(hwnd, nCmdShow); //show window on desktop
	UpdateWindow(hwnd); //update the client area

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}


//Window Procedure
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}