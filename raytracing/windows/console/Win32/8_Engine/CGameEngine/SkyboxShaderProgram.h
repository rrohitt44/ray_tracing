#ifndef H_SHADERPROGRAM_SKYBOX
#define H_SHADERPROGRAM_SKYBOX

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"

void getAllUniformLocationsSkybox(); // get uniform locations
// uniforms
extern GLuint gShaderProgramSkybox;

void loadTextureSamplerSkybox(GLuint value);
void bindTexturesSkybox(struct TexturedModel model);
void loadModelMatrixSkybox(glm::mat4 transformationMatrix);
void loadViewMatrixSkybox(glm::mat4 transformationMatrix);
void loadProjectionMatrixSkybox(glm::mat4 transformationMatrix);
#endif // !H_SHADERPROGRAM_SKYBOX
