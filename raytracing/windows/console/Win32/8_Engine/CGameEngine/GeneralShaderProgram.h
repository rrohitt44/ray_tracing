#ifndef H_SHADERPROGRAM_GENERAL
#define H_SHADERPROGRAM_GENERAL

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"

void getAllUniformLocationsGeneral(); // get uniform locations

// uniforms
extern GLuint gShaderProgramGeneral;

void loadGeneralTextureSampler(GLuint value);
void loadModelMatrixGeneral(glm::mat4 transformationMatrix);
void loadViewMatrixGeneral(glm::mat4 transformationMatrix);
void loadProjectionMatrixGeneral(glm::mat4 transformationMatrix);
void bindTexturesGeneral(struct TexturedModel model);
#endif // !H_SHADERPROGRAM_GENERAL
