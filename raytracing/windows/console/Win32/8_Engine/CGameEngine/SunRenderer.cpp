// headers
#include"SunRenderer.h"

// global variables
extern GLuint gShaderProgramSun;
std::vector<struct Light> lights;
extern struct Camera camera; // for view matrix

/*
The lighting calculations we used so far expect the light direction to be a direction from 
the fragment towards the light source, but people generally prefer to specify a directional light 
as a global direction pointing from the light source. Therefore we have to negate the global 
light direction vector to switch its direction; it's now a direction vector pointing towards the light source.
*/
void setUpDirectionalLight(struct Light* light, struct TexturedModel *texturedModel)
{
	// local variables
	glm::vec3 globalLightDirection(0.2f, 1.0f, 0.3f); // a direction vector

	// code
	light->lightType = 0;
	light->entity.texturedModel = *texturedModel;
	light->entity.translate = glm::vec3(10000, 10000, 10000);
	light->entity.rotateX = 0;
	light->entity.rotateY = 0;
	light->entity.rotateZ = 0;
	light->entity.scale = 0.2f;
	//light->lightColor = glm::vec3(1, 1, 1);

	// for direction lighting - e.g. sun
	// make a global direction vector pointing 'from the light source' by negating it
	// light's direction will be pointing downwards
	globalLightDirection = -globalLightDirection;
	light->lightDirection = globalLightDirection;//light->entity.translate;

	light->ambient = glm::vec3(1);
	light->diffuse = glm::vec3(1);
	light->specular = glm::vec3(1);
}

void setUpPointLight(struct Light* light, struct TexturedModel* texturedModel, glm::vec3 lightPos, glm::vec3 diffuse)
{
	// code
	light->lightType = 1;
	light->entity.texturedModel = *texturedModel;
	light->entity.translate = lightPos;
	light->entity.rotateX = 0;
	light->entity.rotateY = 0;
	light->entity.rotateZ = 0;
	light->entity.scale = 0.2f;
	light->lightColor = glm::vec3(1, 1, 1);

	light->ambient = glm::vec3(0.2f);
	light->diffuse = diffuse;
	light->specular = glm::vec3(1.0f);

	// for point lighting
	//http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
	light->lightPosition = light->entity.translate;
	light->constant = 1.0f;
	light->linear = 0.09f;
	light->quadratic = 0.032f;
}

void setUpSpotLight(struct Light* light, struct TexturedModel* texturedModel)
{
	// code
	light->lightType = 2;
	light->entity.texturedModel = *texturedModel;
	light->entity.translate = camera.position; // glm::vec3(0.2f, 0.0f, -1.0f);
	light->entity.rotateX = 0;
	light->entity.rotateY = 0;
	light->entity.rotateZ = 0;
	light->entity.scale = 0.2f;
	//light->lightColor = glm::vec3(1);

	light->ambient = glm::vec3(0.2f);
	light->diffuse = glm::vec3(0.5f);
	light->specular = glm::vec3(1.0f);

	light->lightPosition = camera.position;
	light->lightDirection = camera.cameraFront;
	light->cutoff = glm::cos(glm::radians(12.5f));
	light->outerCutoff = glm::cos(glm::radians(17.5f));
}

void prepareSunRenderer(glm::mat4 projectionMatrix)
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("sun.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("sun.fs");

	// code
	// create shader program object
	gShaderProgramSun = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("Sun Shader program object created.");

	// Load uniforms
	getAllUniformLocationsSun(); // get the uniform locations
	logStaticData("Loaded Sun uniform locations.");

	startProgram(gShaderProgramSun);
	loadProjectionMatrixSun(projectionMatrix);
	stopProgram();
}

// add entity to the entity list for rendering
void processLight(struct Light light)
{
	lights.push_back(light);
}

void processLights(std::vector<struct Light> l)
{
	lights.insert(lights.begin(),l.begin(), l.end());
}

// render lights
void renderLights()
{
	for (int i = 0; i < lights.size(); i++)
	{
		glm::mat4 transformationMatrix = glm::mat4(1.0);
		// load uniforms
		loadModelMatrixSun(getTransformationMatrix(transformationMatrix, lights[i].entity));
		loadObjectColorSun(lights[i].diffuse);
		// draw entity
		draw(lights[i].entity.texturedModel.rawModel.vertexCount, true);
	}
}

// clean up the shader program object
void cleanUpLights()
{
	lights.clear();
}

