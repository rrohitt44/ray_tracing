#ifndef H_SHADERPROGRAM_COMPUTE
#define H_SHADERPROGRAM_COMPUTE

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"
#include"Camera.h"

void getAllUniformLocationsCompute(); // get uniform locations

// uniforms
extern GLuint gShaderProgramCompute;

void loadTextureSamplerCompute(GLuint value);
void loadModelMatrixCompute(glm::mat4 transformationMatrix);
void loadViewMatrixCompute(glm::mat4 transformationMatrix);
void loadProjectionMatrixCompute(glm::mat4 transformationMatrix);
void bindTexturesCompute(struct TexturedModel model);
void loadWorld(std::vector<struct Sphere> world);
void loadComputeCamera(struct Camera_Compute cameraCompute);
void loadLensRadius(float radius);
#endif // !H_SHADERPROGRAM_COMPUTE
