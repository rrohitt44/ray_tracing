#include"ComputeShaderProgram.h"

using namespace std;

GLuint gShaderProgramCompute;
static GLuint gLocModelMatrix;
static GLuint gLocViewMatrix;
static GLuint gLocProjectionMatrix;
static GLuint gLocTextureSampler;
static GLuint gLocSphereCenter[2];
static GLuint gLocSphereRadius[2];
static GLuint gLocCameraLeftCorner;
static GLuint gLocCameraHorizontal;
static GLuint gLocCameraVertical;
static GLuint gLocCameraOrigin;
static GLuint gLocCameraLensRadius;
static GLuint gLocCameraLensU;
static GLuint gLocCameraLensV;
static GLuint gLocCameraLensW;


// gets all uniform locations
void getAllUniformLocationsCompute()
{
	gLocTextureSampler = glGetUniformLocation(gShaderProgramCompute, "u_texture_sampler");
	gLocModelMatrix = glGetUniformLocation(gShaderProgramCompute, "u_model_matrix");
	gLocViewMatrix = glGetUniformLocation(gShaderProgramCompute, "u_view_matrix");
	gLocProjectionMatrix = glGetUniformLocation(gShaderProgramCompute, "u_projection_matrix");

	// world parameters
	for (int i = 0; i < 2; i++)
	{
		std::string uniformString = "u_spheres[" + std::to_string(i) + "].center";
		gLocSphereCenter[i] = glGetUniformLocation(gShaderProgramCompute, uniformString.c_str());

		uniformString = "u_spheres[" + std::to_string(i) + "].radius";
		gLocSphereRadius[i] = glGetUniformLocation(gShaderProgramCompute, uniformString.c_str());
	}

	// camera parameters
	gLocCameraLeftCorner = glGetUniformLocation(gShaderProgramCompute, "u_camera.lower_left_corner");
	gLocCameraHorizontal = glGetUniformLocation(gShaderProgramCompute, "u_camera.horizontal");
	gLocCameraVertical = glGetUniformLocation(gShaderProgramCompute, "u_camera.vertical");
	gLocCameraOrigin = glGetUniformLocation(gShaderProgramCompute, "u_camera.origin");
	gLocCameraLensRadius = glGetUniformLocation(gShaderProgramCompute, "u_camera.radius");
	gLocCameraLensU = glGetUniformLocation(gShaderProgramCompute, "u_camera.u");
	gLocCameraLensV = glGetUniformLocation(gShaderProgramCompute, "u_camera.v");
	gLocCameraLensW = glGetUniformLocation(gShaderProgramCompute, "u_camera.w");
}

void loadModelMatrixCompute(glm::mat4 transformationMatrix)
{
	setMat4(gLocModelMatrix, transformationMatrix);
}

void loadViewMatrixCompute(glm::mat4 transformationMatrix)
{
	setMat4(gLocViewMatrix, transformationMatrix);
}

void loadProjectionMatrixCompute(glm::mat4 transformationMatrix)
{
	setMat4(gLocProjectionMatrix, transformationMatrix);
}

void loadTextureSamplerCompute(GLuint value)
{
	setInt(gLocTextureSampler, value);
}

void bindTexturesCompute(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, model.textureID);
}

void loadWorld(std::vector<struct Sphere> world)
{
	for (int i = 0; i < 2; i++)
	{
		setVector3v(gLocSphereCenter[i], world[i].center);
		setFloat(gLocSphereRadius[i], world[i].radius);
	}
}

void loadComputeCamera(struct Camera_Compute cameraCompute)
{
	setVector3v(gLocCameraOrigin, cameraCompute.origin);
	setVector3v(gLocCameraLeftCorner, cameraCompute.lower_left_corner);
	setVector3v(gLocCameraHorizontal, cameraCompute.horizontal);
	setVector3v(gLocCameraVertical, cameraCompute.vertical);
	setVector3v(gLocCameraLensU, cameraCompute.u);
	setVector3v(gLocCameraLensV, cameraCompute.v);
	setVector3v(gLocCameraLensW, cameraCompute.w);
}

void loadLensRadius(float radius)
{
	setFloat(gLocCameraLensRadius, radius);
}
