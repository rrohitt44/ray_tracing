#include"FillScreenShaderProgram.h"

using namespace std;

GLuint gShaderProgramFillScreenQuad;
static GLuint gLocTextureSamplerFillScreenQuad;

// gets all uniform locations
void getAllUniformLocationsFillScreenQuad()
{
	gLocTextureSamplerFillScreenQuad = glGetUniformLocation(gShaderProgramFillScreenQuad, "u_texture_sampler");
}

void loadTextureSamplerFillScreenQuad(GLuint value)
{
	setInt(gLocTextureSamplerFillScreenQuad, value);
}

void bindTexturesFillScreenQuad(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, model.textureID);
}