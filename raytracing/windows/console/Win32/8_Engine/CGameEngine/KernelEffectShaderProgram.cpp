#include"KernelEffectShaderProgram.h"

using namespace std;

GLuint gShaderProgramKernelEffect;
static GLuint gLocTextureSamplerKernelEffect;

// gets all uniform locations
void getAllUniformLocationsKernelEffect()
{
	gLocTextureSamplerKernelEffect = glGetUniformLocation(gShaderProgramKernelEffect, "u_texture_sampler");
}

void loadTextureSamplerKernelEffect(GLuint value)
{
	setInt(gLocTextureSamplerKernelEffect, value);
}

void bindTexturesKernelEffect(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, model.textureID);
}