#ifndef H_RAYTRACED_RENDERER
#define H_RAYTRACED_RENDERER

#include<iostream>
#include<string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include"MathUtils.h"
#include"DisplayManager.h"
#include"ComputeRenderer.h"
#include"GeneralRenderer.h"

extern GLuint gShaderProgramGeneral;
extern std::vector<struct Entity> gGeneralEntities;
extern glm::mat4 gProjectionMatrix;
extern struct Texture computeTexture;

void prepareRayTracedRenderer();
void renderRayTraced(struct Camera* camera);

#endif // !H_RAYTRACED_RENDERER
