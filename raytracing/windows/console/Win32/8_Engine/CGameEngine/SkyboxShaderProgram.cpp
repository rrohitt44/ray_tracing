#include"SkyboxShaderProgram.h"

using namespace std;

GLuint gShaderProgramSkybox;
static GLuint gLocTextureSamplerSkybox;
static GLuint gLocModelMatrixSkybox;
static GLuint gLocViewMatrixSkybox;
static GLuint gLocProjectionMatrixSkybox;

// gets all uniform locations
void getAllUniformLocationsSkybox()
{
	gLocTextureSamplerSkybox = glGetUniformLocation(gShaderProgramSkybox, "u_texture_sampler");
	gLocModelMatrixSkybox = glGetUniformLocation(gShaderProgramSkybox, "u_model_matrix");
	gLocViewMatrixSkybox = glGetUniformLocation(gShaderProgramSkybox, "u_view_matrix");
	gLocProjectionMatrixSkybox = glGetUniformLocation(gShaderProgramSkybox, "u_projection_matrix");
}

void loadTextureSamplerSkybox(GLuint value)
{
	setInt(gLocTextureSamplerSkybox, value);
}

void loadModelMatrixSkybox(glm::mat4 transformationMatrix)
{
	setMat4(gLocModelMatrixSkybox, glm::mat4(glm::mat3(transformationMatrix)));
}

void loadViewMatrixSkybox(glm::mat4 transformationMatrix)
{
	setMat4(gLocViewMatrixSkybox, glm::mat4(glm::mat3(transformationMatrix)));
	//setMat4(gLocViewMatrixSkybox, transformationMatrix);
}

void loadProjectionMatrixSkybox(glm::mat4 transformationMatrix)
{
	setMat4(gLocProjectionMatrixSkybox, transformationMatrix);
}

void bindTexturesSkybox(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, model.textureID);
}