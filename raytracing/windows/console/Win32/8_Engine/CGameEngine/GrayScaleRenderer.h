#ifndef H_INVERSION_GRAYSCALE
#define H_INVERSION_GRAYSCALE

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"GrayScaleShaderProgram.h"
#include"Camera.h"
#include"framebuffers.h"
#include"PostProcessing.h"

void prepareGrayScale(struct FBO* inversionFbo);
void renderGrayScale(struct FBO* fbo, struct FBO* inversionFbo); // render entities using FBO textures
#endif // !H_INVERSION_RENDERER
