// headers
#include"FillScreenRenderer.h"

// global variables
extern GLuint gShaderProgramFillScreenQuad;
struct Entity fillScreenQuadEntity;

void prepareFillScreenQuadRenderer()
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("fillScreenQuad.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("fillScreenQuad.fs");

	// code
	// create shader program object
	gShaderProgramFillScreenQuad = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("Fill Screen Quad Shader program object created.");

	// Load uniforms
	getAllUniformLocationsFillScreenQuad(); // get the uniform locations
	logStaticData("Loaded Fill Screen Quad uniform locations.");

	startProgram(gShaderProgramFillScreenQuad);
	loadTextureSamplerFillScreenQuad(0);
	stopProgram();

}

void processFillScreenQuadEntity(struct Entity* entity)
{
	fillScreenQuadEntity = *entity;
}

// render lights
void renderFillScreenQuad(struct FBO* fbo)
{
	glBindVertexArray(fillScreenQuadEntity.texturedModel.rawModel.vaoID);
	fillScreenQuadEntity.texturedModel.textureID = fbo->colorAttachment;
		bindTexturesFillScreenQuad(fillScreenQuadEntity.texturedModel);
		// draw entity
		draw(fillScreenQuadEntity.texturedModel.rawModel.vertexCount, true);
		glBindVertexArray(0);
		
}

void renderFillScreenQuadAfterPostprocessing(GLuint textureId)
{
	unbindFbo();
	startProgram(gShaderProgramFillScreenQuad);
	glBindVertexArray(fillScreenQuadEntity.texturedModel.rawModel.vaoID);
	fillScreenQuadEntity.texturedModel.textureID = textureId;
	bindTexturesFillScreenQuad(fillScreenQuadEntity.texturedModel);
	// draw entity
	draw(fillScreenQuadEntity.texturedModel.rawModel.vertexCount, true);
	glBindVertexArray(0);
	stopProgram();
	stopPostProcessing();
}