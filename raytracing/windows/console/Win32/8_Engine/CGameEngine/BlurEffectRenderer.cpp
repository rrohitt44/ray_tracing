// headers
#include"BlurEffectRenderer.h"

// global variables
extern GLuint gShaderProgramBlurEffect;
extern struct Entity fillScreenQuadEntity;

void prepareBlurEffect(struct FBO * fbo)
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("fillScreenQuad.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("blur_effect.fs");

	// code
	// create shader program object
	gShaderProgramBlurEffect = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("Blur Shader program object created.");

	// Load uniforms
	getAllUniformLocationsBlurEffect(); // get the uniform locations
	logStaticData("Loaded Blur uniform locations.");

	// create FBo for storing inversion buffers
	setUpFrameBuffer(fbo);

	// load uniform data
	startProgram(gShaderProgramBlurEffect);
	loadTextureSamplerBlurEffect(0);
	stopProgram();

}


void renderBlurEffect(struct FBO* inputFbo, struct FBO* outputFbo)
{
	bindFbo(outputFbo->fboId);
	startPostProcessing();
	//unbindFbo();
	startProgram(gShaderProgramBlurEffect);
	glBindVertexArray(fillScreenQuadEntity.texturedModel.rawModel.vaoID);
	fillScreenQuadEntity.texturedModel.textureID = inputFbo->colorAttachment;
		bindTexturesInversion(fillScreenQuadEntity.texturedModel);
		// draw entity
		draw(fillScreenQuadEntity.texturedModel.rawModel.vertexCount, true);
		glBindVertexArray(0);
		stopProgram();
		unbindFbo();
		stopPostProcessing();
}