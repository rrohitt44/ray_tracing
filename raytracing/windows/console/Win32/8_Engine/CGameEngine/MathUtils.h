#ifndef H_MATH_UTILS
#define H_MATH_UTILS

#define PI 3.14159

/*
https://learnopengl.com/Getting-started/Transformations for transformation, scaling and rotation of the matrices
*/
// headers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include"Models.h"

glm::mat4 creatTranslationMatrix(glm::mat4 translationMatrix, glm::vec3 translateBy);
glm::mat4 createOrthographicProjectionMatrix();
glm::mat4 getTransformationMatrix(glm::mat4 transformationMatrix, struct Entity entity);
glm::mat4 getTransformationMatrixEntityMaps(glm::mat4 transformationMatrix, struct EntityMap entity);
glm::mat4 createPerspectiveProjectionMatrix(float fov, int width, int height);
glm::mat4 createViewMatrix(struct Camera camera);
float toRadians(float value);
float toDegrees(float value);
float getNextFloatRandomNumberBetween0And1();
#endif // !H_MATH_UTILS
