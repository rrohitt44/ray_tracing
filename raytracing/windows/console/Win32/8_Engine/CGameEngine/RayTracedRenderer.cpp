// headers
#include"RayTracedRenderer.h"

void prepareRayTracedRenderer()
{
	// create projection matrix
	float fov = 45.0f;
	gProjectionMatrix = createPerspectiveProjectionMatrix(gFov, WIN_WIDTH, WIN_HEIGHT);
	prepareGeneralRenderer(gProjectionMatrix);
	prepareComputeRenderer(gProjectionMatrix);
}

void renderRayTraced(struct Camera* camera)
{
	// launch compute shaders!
	renderCompute();

	// normal rendering
	// render general entitieis
	// start entity shader program
	startProgram(gShaderProgramGeneral);
	if (gGeneralEntities.size() > 0)
	{
		loadProjectionMatrixGeneral(gProjectionMatrix);
		// bind VAO
		glBindVertexArray(gGeneralEntities[0].texturedModel.rawModel.vaoID);
		gGeneralEntities[0].texturedModel.textureID = computeTexture.textureID;
		renderGeneral(camera);
		// unbind VAO
		glBindVertexArray(0);
	}
	// stop shader program
	stopProgram(); // general shader program ended
}