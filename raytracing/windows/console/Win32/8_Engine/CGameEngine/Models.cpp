// headers
#include"Models.h"

// function prototype declarations
void setUpCubeMesh(struct MeshModel* mesh);
void setUpCubeMapMesh(struct MeshModel* mesh);
void setUpQuadMesh(struct MeshModel* mesh);
void setUpFillScreenQuadMesh(struct MeshModel* mesh);

void setUpCubeMesh(struct MeshModel* mesh)
{
	float positions[]
	{
		-0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,

		-0.5f, -0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,
		-0.5f, -0.5f,  0.5f,

		-0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,
		-0.5f, -0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,

		 0.5f,  0.5f,  0.5f,
		 0.5f,  0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f,

		-0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f,
		-0.5f, -0.5f,  0.5f,
		-0.5f, -0.5f, -0.5f,

		-0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f, -0.5f
		/*-1, -1, -1,
		1, -1, -1,
		1, 1, -1,
		-1, 1, -1,
		-1, -1, 1,
		1, -1, 1,
		1, 1, 1,
		-1, 1, 1*/
	};

	float skyboxVertices[] = {
		// positions          
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f
	};

	float normals[]
	{
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,

		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,

		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,

		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,

		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,

		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f
		/*0, 0, 1,
		1, 0, 0,
		0, 0, -1,
		-1, 0, 0,
		0, 1, 0,
		0, -1, 0*/
	};

	float texCoords[]
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,

		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,

		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f
	};

	unsigned int indices[]
	{
		0,1,2,	3,4,5,6,
		7,8,9,	10,11,12,
		13,14,15,	16,17,18,
		19,20,21,	22,23,24,
		25,26,27,	28,29,30,
		31,32,33,	34,35
		/*0, 1, 3, 3, 1, 2,
	1, 5, 2, 2, 5, 6,
	5, 4, 6, 6, 4, 7,
	4, 0, 7, 7, 0, 3,
	3, 2, 7, 7, 2, 6,
	4, 5, 0, 0, 5, 1*/
	};

	// set up the mesh object from this data
	int numVertices = (sizeof(positions) / sizeof(positions[0]))/3;
	int numIndices = sizeof(indices) / sizeof(indices[0]);
	setUpMesh(mesh, numVertices, positions, normals, texCoords, numIndices, indices);
}

void setUpCubeMapMesh(struct MeshModel* mesh)
{
	float positions[] = {
		// positions          
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f
	};

	float normals[]
	{
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,

		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,

		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,

		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,

		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,

		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f
		/*0, 0, 1,
		1, 0, 0,
		0, 0, -1,
		-1, 0, 0,
		0, 1, 0,
		0, -1, 0*/
	};

	float texCoords[]
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,

		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,

		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f
	};

	unsigned int indices[]
	{
		0,1,2,	3,4,5,6,
		7,8,9,	10,11,12,
		13,14,15,	16,17,18,
		19,20,21,	22,23,24,
		25,26,27,	28,29,30,
		31,32,33,	34,35
		/*0, 1, 3, 3, 1, 2,
	1, 5, 2, 2, 5, 6,
	5, 4, 6, 6, 4, 7,
	4, 0, 7, 7, 0, 3,
	3, 2, 7, 7, 2, 6,
	4, 5, 0, 0, 5, 1*/
	};

	// set up the mesh object from this data
	int numVertices = (sizeof(positions) / sizeof(positions[0])) / 3;
	int numIndices = sizeof(indices) / sizeof(indices[0]);
	setUpMesh(mesh, numVertices, positions, normals, texCoords, numIndices, indices);
}

/*
loadCubeToVAO:
Creates Mesh from cube data and load the data to VAO
*/
void loadCubeToVAO(struct RawModel* rawModel, bool isCubemap)
{
	// local variables
	struct MeshModel mesh;

	// code
	if (isCubemap)
	{
		setUpCubeMapMesh(&mesh);
	}
	else
	{
		setUpCubeMesh(&mesh);
	}
	

	// out mesh is ready with the data now
	// setup the VAO
	rawModel->vaoID = setUpVAO(&mesh);
	rawModel->vertexCount = mesh.indices.size();
}

void setUpQuadMesh(struct MeshModel* mesh)
{
	// local variables
	// position data
	float positions[]
	{
		 0.5f,  0.5f, 0.0f,
		 0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
		-0.5f,  0.5f, 0.0f
	};

	// normals data
	float normals[]
	{
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f
	};

	// texture coordinates
	float texCoords[]
	{
		2.0f, 2.0f,
		2.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 2.0f
	};

	// indices
	unsigned int indices[]
	{
		0, 1, 3, // first triangle
		1, 2, 3  // second triangle
	};

	// set up the mesh object from this data
	int numVertices = (sizeof(positions) / sizeof(positions[0]))/3;
	int numIndices = sizeof(indices) / sizeof(indices[0]);
	setUpMesh(mesh, numVertices, positions, normals, texCoords, numIndices, indices);
}

void setUpFillScreenQuadMesh(struct MeshModel* mesh)
{
	// local variables
	// position data
	float positions[]
	{
		-1.0f,  1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,

		-1.0f,  1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f
	};

	// normals data
	float normals[]
	{
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f
	};

	// texture coordinates
	float texCoords[]
	{
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f
	};

	// indices
	unsigned int indices[]
	{
		0, 1, 2, // first triangle
		3, 4, 5  // second triangle
	};

	// set up the mesh object from this data
	int numVertices = (sizeof(positions) / sizeof(positions[0])) / 3;
	int numIndices = sizeof(indices) / sizeof(indices[0]);
	setUpMesh(mesh, numVertices, positions, normals, texCoords, numIndices, indices);
}

/*
loadQuadToVAO:
Creates Mesh from quad data and load the data to VAO
*/
void loadQuadToVAO(struct RawModel* rawModel, bool isFillScreenQuadRequested)
{
	// local variables
	struct MeshModel mesh;

	// code
	if (isFillScreenQuadRequested)
	{
		setUpFillScreenQuadMesh(&mesh);
	}
	else
	{
		setUpQuadMesh(&mesh);
	}
	

	// our mesh is ready with the data now
	// setup the VAO
	rawModel->vaoID = setUpVAO(&mesh);
	rawModel->vertexCount = mesh.indices.size();
}

GLuint loadSkybox()
{
	std::vector<std::string> skyboxFaces
	{
		IMAGES_RESOURCE_FILE_LOC+"skybox\\"+"right.jpg",
		IMAGES_RESOURCE_FILE_LOC + "skybox\\" + "left.jpg",
		IMAGES_RESOURCE_FILE_LOC + "skybox\\" + "top.jpg",
		IMAGES_RESOURCE_FILE_LOC + "skybox\\" + "bottom.jpg",
		IMAGES_RESOURCE_FILE_LOC + "skybox\\" + "front.jpg",
		IMAGES_RESOURCE_FILE_LOC + "skybox\\" + "back.jpg"
	};

	GLuint cubemapId = loadCubemap(skyboxFaces);
	return cubemapId;
}

// generates the terrain in the world at gridX and gridZ position
void generateTerrainModel(int gridX, int gridZ, struct RawModel* terrainModel)
{
	// local variables
	const int SIZE = 800;
	
	int vertexPointer = 0; // for tracking count of the vertices
	struct MeshModel terrainMesh;
	const string heightmapImageFile = IMAGES_RESOURCE_FILE_LOC + string("heightmap.png");
	int imageWidth, imageHeight, nrChannels;
	int desiredChannelCount = 3;

	// code
	// load the image
	unsigned char* imageData = stbi_load(
		heightmapImageFile.c_str(), // file to be loaded as a texture
		&imageWidth, // width of the image
		&imageHeight, // height of the image
		&nrChannels, // number of channels of the image
		desiredChannelCount
	);

	int VERTEX_COUNT = imageHeight;
	const int count = VERTEX_COUNT * VERTEX_COUNT; // n*n grid
	const int VERTEX_ARRAY_SIZE = count * 3; // size of the vertex array
	const int NORMAL_ARRAY_SIZE = count * 3; // size of the normal array
	const int TEXCOORD_ARRAY_SIZE = count * 2; // size of the tex coords array
	const int INDICES_ARRAY_SIZE = 6 * (VERTEX_COUNT - 1) * (VERTEX_COUNT - 1); // size of indices array
	//float vertices[VERTEX_ARRAY_SIZE]; // for storing vertex data
	//float normals[NORMAL_ARRAY_SIZE]; // for storing normal data
	//float texCoords[TEXCOORD_ARRAY_SIZE]; // stores the texture coordinates
	//unsigned int indices[INDICES_ARRAY_SIZE]; // indices array
	std::vector<float> vertices(VERTEX_ARRAY_SIZE);
	std::vector<float> normals(NORMAL_ARRAY_SIZE);
	std::vector<float> texCoords(TEXCOORD_ARRAY_SIZE);
	std::vector<int> indices(INDICES_ARRAY_SIZE);
	std::vector<std::vector<float>> terrainHeights;

	for (int i = 0; i < VERTEX_COUNT; i++)
	{
		for (int j = 0; j < VERTEX_COUNT; j++)
		{
			// generate vertex position data
			vertices[vertexPointer * 3] = -(float)j / ((float)VERTEX_COUNT - 1) * SIZE;
			float height = 0.1;//getHeightAtPixel(i, j, imageData, desiredChannelCount, imageHeight);
			//terrainHeights[i][j] = height;
			//vertices[vertexPointer * 3 + 1] = 0; // for plain terrain
			vertices[vertexPointer * 3 + 1] = height;
			vertices[vertexPointer * 3 + 2] = -(float)i / ((float)VERTEX_COUNT - 1) * SIZE;

			// generate vertex normal data
			glm::vec3 norm = glm::vec3(0,1,0);//calculateNormal(i, j, imageData, desiredChannelCount, imageHeight);
			normals[vertexPointer * 3] = norm.x;
			normals[vertexPointer * 3 + 1] = norm.y;
			normals[vertexPointer * 3 + 2] = norm.z;

			// generate texture coordinates
			texCoords[vertexPointer * 2] = (float)j / ((float)VERTEX_COUNT - 1); // s
			texCoords[vertexPointer * 2 + 1] = (float)i / ((float)VERTEX_COUNT - 1); // t

			// increament vertex pointer now
			vertexPointer++;
		}
	}

	// generate indices
	int pointer = 0;
	for (int gz = 0; gz < VERTEX_COUNT - 1; gz++)
	{
		for (int gx = 0; gx < VERTEX_COUNT - 1; gx++)
		{
			int topLeft = gz * VERTEX_COUNT + gx;
			int topRight = topLeft + 1;
			int bottomLeft = ((gz + 1) * VERTEX_COUNT) + gx;
			int bottomRight = bottomLeft + 1;
			indices[pointer++] = topLeft;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = topRight;
			indices[pointer++] = topRight;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = bottomRight;
		}
	}

	// set up the mesh object from this data
	setUpMeshFromVecWithIndices(&terrainMesh, vertices, normals, texCoords, indices);

	// our mesh is ready with the data now
	// setup the VAO
	terrainModel->vaoID = setUpVAO(&terrainMesh);
	terrainModel->vertexCount = terrainMesh.indices.size();
}