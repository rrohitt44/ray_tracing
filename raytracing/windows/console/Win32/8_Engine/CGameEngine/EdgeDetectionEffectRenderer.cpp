// headers
#include"EdgeDetectionEffectRenderer.h"

// global variables
extern GLuint gShaderProgramEdgeDetectionEffect;
extern struct Entity fillScreenQuadEntity;

void prepareEdgeDetectionEffect(struct FBO * fbo)
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("fillScreenQuad.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("edge_detection_effect.fs");

	// code
	// create shader program object
	gShaderProgramEdgeDetectionEffect = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("Blur Shader program object created.");

	// Load uniforms
	getAllUniformLocationsEdgeDetectionEffect(); // get the uniform locations
	logStaticData("Loaded Blur uniform locations.");

	// create FBo for storing inversion buffers
	setUpFrameBuffer(fbo);

	// load uniform data
	startProgram(gShaderProgramEdgeDetectionEffect);
	loadTextureSamplerEdgeDetectionEffect(0);
	stopProgram();

}


void renderEdgeDetectionEffect(struct FBO* inputFbo, struct FBO* outputFbo)
{
	bindFbo(outputFbo->fboId);
	startPostProcessing();
	//unbindFbo();
	startProgram(gShaderProgramEdgeDetectionEffect);
	glBindVertexArray(fillScreenQuadEntity.texturedModel.rawModel.vaoID);
	fillScreenQuadEntity.texturedModel.textureID = inputFbo->colorAttachment;
		bindTexturesInversion(fillScreenQuadEntity.texturedModel);
		// draw entity
		draw(fillScreenQuadEntity.texturedModel.rawModel.vertexCount, true);
		glBindVertexArray(0);
		stopProgram();
		unbindFbo();
		stopPostProcessing();
}