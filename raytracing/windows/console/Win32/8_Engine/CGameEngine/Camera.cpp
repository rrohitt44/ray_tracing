// header
#include"Camera.h"

// global variables
struct Camera camera;
struct Camera_Compute cameraCompute;

glm::vec3 gCameraPosition(camera.position);

float distanceFromPlayer = 10;
float angleAroundPlayer = 0;
float pitchCam = 20;
float yawCam = 0;
float rollCam = 0;

// compute camera
glm::vec3 lookFromCC;
glm::vec3 lookAtCC;
glm::vec3 vupCC;
float vFovCC;
float aspectCC;

void initializeCamera()
{
	camera.position = glm::vec3(0,-3.4,3);//player.translate +glm::vec3(0, 0.5, 10);
	updateCameraPosition(camera.position);
		camera.cameraFront = glm::vec3(0.0, 0.0, -1.0); // in front of camera
	camera.up = glm::vec3(0.0, 1.0, 0.0); // up axis

	gCameraPosition = camera.position;
	logStaticData("Camera initialized");
}

void initializeCCCamera()
{
	lookFromCC = glm::vec3(2, 3,2);
	lookAtCC = glm::vec3(0.0, 0.0, -1.0);
	vupCC = glm::vec3(0.0, 1.0, 0.0);
	vFovCC = 90.0f;
	updateComputeCamera(lookFromCC, lookAtCC, vupCC, vFovCC, aspectCC);
}
void updateCameraPosition(glm::vec3 position)
{
	if (gAKeyPressed == true)
	{
		//gCurrentTurnSpeed = PLAYER_TURN_SPEED;
		angleAroundPlayer -= 0.5f;
	}
	else if (gDKeyPressed == true)
	{
		//gCurrentTurnSpeed = -PLAYER_TURN_SPEED;
		angleAroundPlayer += 0.5f;
	}
	else
	{
		//angleAroundPlayer = 0;
	}

	float dy = distanceFromPlayer * sin(toRadians(pitchCam));
	float dx = distanceFromPlayer * cos(toRadians(pitchCam));

	float theta = player.rotateY + angleAroundPlayer;
	float offsetX = dx * sinf(toRadians(theta));
	float offsetZ = dy * cosf(toRadians(theta));
	camera.position.x = player.translate.x - offsetX;
	camera.position.z = player.translate.z - offsetZ;
	camera.position.y = player.translate.y + dy; // position
	yawCam = 180 - theta;

	yawCam = yawCam + offsetX; 
	pitchCam = pitchCam + offsetZ;

	/*if (pitchCam > 89.0f)
		pitchCam = 89.0f;
	if (pitchCam < -89.0f)
		pitchCam = -89.0f;*/

	/*glm::vec3 front(0,0,0);
	front.x = cos(glm::radians(yawCam)) * cos(glm::radians(yawCam));
	front.y = sin(glm::radians(pitchCam));
	front.z = sin(glm::radians(yawCam)) * cos(glm::radians(pitchCam));
	camera.cameraFront = glm::normalize(front);*/
	//position.z += player.translate.z + 4;
	//position.y += player.translate.y + 3;
	camera.position += position;
}

void updateComputeCamera(glm::vec3 lookFrom, glm::vec3 lookAt, glm::vec3 vup, float vFov, float aspect)
{
	float focus_to_dist = glm::length(lookFrom - lookAt);
	float M_PI = 3.14;
	
	glm::vec3 u, v, w;
	float theta = vFov * M_PI / 180.0f;
	float halfHeight = tan(theta / 2);
	float halfWidth = aspect * halfHeight;

	cameraCompute.origin = lookFrom;
	w = glm::normalize(lookFrom - lookAt);
	u = glm::normalize(glm::cross(vup, w));
	v = glm::cross(w, u);

	cameraCompute.u = u;
	cameraCompute.v = v;
	cameraCompute.w = w;
	//cameraCompute.lower_left_corner = glm::vec3(-halfWidth, -halfHeight, -1.0);
	cameraCompute.lower_left_corner = cameraCompute.origin - halfWidth * focus_to_dist*u 
		- halfHeight* focus_to_dist *v - w * focus_to_dist;
	cameraCompute.lower_left_corner = glm::vec3(0,0, cameraCompute.lower_left_corner.z);
	cameraCompute.horizontal = 2 * halfWidth * u * focus_to_dist;
	cameraCompute.vertical = 2 * halfHeight * v * focus_to_dist;
}