#include"PostProcessing.h"

struct FBO inversionFbo;
struct FBO grayScaleFbo;
struct FBO kernelEffectFbo;
struct FBO blurEffectFbo;
struct FBO edgeDetectionFbo;

void preparePostProcessing()
{
	prepareInversion(&inversionFbo);
	prepareGrayScale(&grayScaleFbo);
	prepareKernelEffect(&kernelEffectFbo);
	prepareBlurEffect(&blurEffectFbo);
	prepareEdgeDetectionEffect(&edgeDetectionFbo);
}

void doPostProcessing(struct FBO *fbo)
{
	//glViewport(0, 0, gWindowWidth, gWindowHeight);
	//renderInversion(fbo, &inversionFbo);
	//renderGrayScale(fbo, &grayScaleFbo);
	//renderKernelEffect(fbo, &kernelEffectFbo);
	//renderBlurEffect(fbo, &blurEffectFbo);
	//renderEdgeDetectionEffect(&grayScaleFbo, &edgeDetectionFbo);
	renderFillScreenQuadAfterPostprocessing(fbo->colorAttachment);
}

void startPostProcessing()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	disableDepthTesting();
}

void stopPostProcessing()
{
	enableDepthTesting();
}