#include"GeneralShaderProgram.h"

using namespace std;

GLuint gShaderProgramSingleColor;
static GLuint gLocModelMatrix;
static GLuint gLocViewMatrix;
static GLuint gLocProjectionMatrix;
static GLuint gLocTextureSampler;

// gets all uniform locations
void getAllUniformLocationsSingleColor()
{
	gLocTextureSampler = glGetUniformLocation(gShaderProgramSingleColor, "u_texture_sampler");
	gLocModelMatrix = glGetUniformLocation(gShaderProgramSingleColor, "u_model_matrix");
	gLocViewMatrix = glGetUniformLocation(gShaderProgramSingleColor, "u_view_matrix");
	gLocProjectionMatrix = glGetUniformLocation(gShaderProgramSingleColor, "u_projection_matrix");
}

void loadModelMatrixSingleColor(glm::mat4 transformationMatrix)
{
	setMat4(gLocModelMatrix, transformationMatrix);
}

void loadViewMatrixSingleColor(glm::mat4 transformationMatrix)
{
	setMat4(gLocViewMatrix, transformationMatrix);
}

void loadProjectionMatrixSingleColor(glm::mat4 transformationMatrix)
{
	setMat4(gLocProjectionMatrix, transformationMatrix);
}

void loadSingleColorTextureSampler(GLuint value)
{
	setInt(gLocTextureSampler, value);
}

void bindTexturesSingleColor(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, model.textureID);
}