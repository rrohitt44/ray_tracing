#ifndef H_LIGHT
#define H_LIGHT

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"SunShaderProgram.h"
#include"Camera.h"

extern std::vector<struct Light> lights;

void setUpDirectionalLight(struct Light*light, struct TexturedModel *texturedModel);
void setUpPointLight(struct Light* light, struct TexturedModel* texturedModel, glm::vec3 lightPos, glm::vec3 diffuse);
void setUpSpotLight(struct Light* light, struct TexturedModel* texturedModel);
void prepareSunRenderer(glm::mat4 projectionMatrix);
void processLight(struct Light sun); // add entity to the entity list for rendering
void processLights(std::vector<struct Light> lights);
void renderLights(); // render entities
void cleanUpLights(); // clean up the shader program object
#endif // !H_LIGHT
