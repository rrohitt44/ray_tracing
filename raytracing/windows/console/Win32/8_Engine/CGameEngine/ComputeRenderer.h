#ifndef H_COPMUTE_RENDERER
#define H_COPMUTE_RENDERER

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"ComputeShaderProgram.h"
#include"Camera.h"

extern struct Texture computeTexture;

void prepareComputeRenderer(glm::mat4 projectionMatrix);
void renderCompute(); // render entities
void renderComputeUsingFBO(struct Camera* camera, struct FBO *fbo); // render entities using FBO textures
#endif // !H_COPMUTE_RENDERER
