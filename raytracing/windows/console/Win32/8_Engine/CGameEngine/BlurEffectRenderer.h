#ifndef H_BLUREFFECT_RENDERER
#define H_BLUREFFECT_RENDERER

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"BlurEffectShaderProgram.h"
#include"Camera.h"
#include"framebuffers.h"
#include"PostProcessing.h"

void prepareBlurEffect(struct FBO* fbo);
void renderBlurEffect(struct FBO* inputFbo, struct FBO* outputFbo); // render entities using FBO textures
#endif // !H_BLUREFFECT_RENDERER
