// headers
#include"ComputeRenderer.h"

// global variables
extern GLuint gShaderProgramCompute;
extern struct Camera_Compute cameraCompute;
struct Texture computeTexture;

void prepareComputeRenderer(glm::mat4 projectionMatrix)
{
	// local variables
	const std::string computeFile = SHADER_RESOURCE_FILE_LOC + std::string("raytracer.coms");

	// code
	/*
	It is up to us how we define and divide up our pile of work to do between
	compute shader invocations. First we should check what the maximum size of the total
	work group that we give to glDispatchCompute() is. We can get the x, y, and z extents of this
	*/
	int work_grp_cnt[3];

	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &work_grp_cnt[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &work_grp_cnt[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &work_grp_cnt[2]);

	std::string debugStirng = "COMPUTERSHADER::max global (total) work group size x:" + std::to_string(work_grp_cnt[0]) +
		" y:" + std::to_string(work_grp_cnt[1]) + " z:" + std::to_string(work_grp_cnt[2]);
	logString(debugStirng);

	/*
	We can also check the maximum size of a local work group (sub-division of the total number of jobs).
	This is defined in the compute shader itself, with the layout qualifier.
	*/
	int work_grp_size[3];

	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &work_grp_size[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &work_grp_size[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &work_grp_size[2]);

	debugStirng = "COMPUTERSHADER::max global (total) work group size x:" + std::to_string(work_grp_size[0]) +
		" y:" + std::to_string(work_grp_size[1]) + " z:" + std::to_string(work_grp_size[2]);
	logString(debugStirng);

	/*
	We can also determine the maximum number of work group units that a local work group
	in the compute shader is allowed. This means that if we process a 32x32 tile of jobs in
	one local work group, then the product (1024) must also not exceed this value.
	*/
	int work_grp_inv;
	glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &work_grp_inv);
	printf("max local work group invocations %i\n", work_grp_inv);
	debugStirng = "COMPUTERSHADER::max local work group invocations:" + std::to_string(work_grp_inv);
	logString(debugStirng);

	// create shader program object
	gShaderProgramCompute = buildComputeShaderProgramObject(computeFile.c_str());
	logStaticData("Compute Shader program object created.");

	// create empty texture to fill data using compute shader
	computeTexture.width = 512;
	computeTexture.height = 512;
	computeTexture.internalFormat = GL_RGBA32F;
	computeTexture.dataFormat = GL_RGBA;
	createEmpty2DTexture(&computeTexture);

	//OpenGL treats "image units" slightly differently to textures, 
		//so we call a glBindImageTexture() function to make this link
	glBindImageTexture(0, computeTexture.textureID, 0, GL_FALSE, 0, GL_WRITE_ONLY, computeTexture.internalFormat);

	// Create World and Camera
	std::vector<struct Sphere> world;
	Sphere sphere;
	sphere.center = glm::vec3(0.0, 0.0, -1.0);
	sphere.radius = 0.8;

	Sphere sphereGround;
	sphereGround.center = glm::vec3(0.0, -100.5, -1.0);
	sphereGround.radius = 100.0;
	world.push_back(sphereGround);
	world.push_back(sphere);

	aspectCC = float(computeTexture.width) / float(computeTexture.height);
	initializeCCCamera();
	float lens_radius = 2.0 / 2; // aperture/2

	// load uniforms
	getAllUniformLocationsCompute();

	// pass uniforms
	startProgram(gShaderProgramCompute);
	loadWorld(world);
	loadComputeCamera(cameraCompute);
	stopProgram();
}

// render lights
void renderCompute()
{
	// launch compute shaders!
	startProgram(gShaderProgramCompute); // start program
	loadComputeCamera(cameraCompute); // update compute camera
	// Note that the compute shader dispatch looks as if it's another drawing pass
	glDispatchCompute(
		computeTexture.width, // num groups x
		computeTexture.height, // num groups y
		1 // num groups z
	);

	// make sure writing to image has finished before read
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	stopProgram(); // stop program
}

void renderComputeUsingFBO(struct Camera* camera, struct FBO *fbo)
{
	/*loadViewMatrixGeneral(createViewMatrix(*camera));
	for (int i = 0; i < gGeneralEntities.size(); i++)
	{
		glm::mat4 transformationMatrix = glm::mat4(1.0);
		gGeneralEntities[i].texturedModel.textureID = fbo->colorAttachment;
		// load uniforms
		loadModelMatrixGeneral(getTransformationMatrix(transformationMatrix, gGeneralEntities[i]));
		bindTexturesGeneral(gGeneralEntities[i].texturedModel);
		// draw entity
		draw(gGeneralEntities[i].texturedModel.rawModel.vertexCount, true);
	}*/
}