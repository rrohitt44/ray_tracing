#version 330 core

layout (location = 0) in vec3 aPosition;
layout (location = 2) in vec2 aTexCoords;

out vec2 out_tex_coords;

void main()
{
    gl_Position =   vec4(aPosition.x, aPosition.y, 0.0, 1.0);
	out_tex_coords = aTexCoords;
}