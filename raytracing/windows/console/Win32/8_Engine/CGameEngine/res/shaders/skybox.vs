#version 330 core

layout (location = 0) in vec3 aPosition;;

out vec3 out_tex_coords;

uniform mat4 u_view_matrix;
uniform mat4 u_projection_matrix;
uniform mat4 u_model_matrix;

void main()
{
    vec4 pos =   u_projection_matrix * u_view_matrix * vec4(aPosition, 1.0);
	gl_Position = pos.xyww;
	out_tex_coords = aPosition;
}