#version 450 core

in vec2 out_tex_coords;

out vec4 FragColor;

uniform sampler2D u_texture_sampler;

void main()
{
	FragColor = texture(u_texture_sampler, out_tex_coords);
}