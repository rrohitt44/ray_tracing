#version 330 core

in vec2 out_tex_coords;

out vec4 FragColor;

uniform sampler2D u_texture_sampler;

void main()
{
	FragColor = vec4(vec3(1.0 - texture(u_texture_sampler, out_tex_coords)), 1.0);
}