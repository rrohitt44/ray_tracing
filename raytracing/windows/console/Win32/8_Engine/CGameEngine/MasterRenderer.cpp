// headers
#include"MasterRenderer.h"

using namespace std;

// function prototype declarations
void renderScene(std::vector<struct Light> lights, struct Camera* camera); // render entities

// global variables
// for entities
extern std::vector<Entity> entities;
extern GLuint gShaderProgramEntity;

// for lights
extern GLuint gShaderProgramSun;
extern std::vector<struct Light> lights;

// for entities with maps
extern std::vector<EntityMap> entityMaps;
extern std::vector<Mesh> gLoadedModels;;
extern GLuint gShaderProgramEntityMaps;

extern std::vector<EntityMap> terrains;
extern GLuint gShaderProgramTerrain;

extern GLuint gShaderProgramGeneral;
extern std::vector<struct Entity> gGeneralEntities;

extern GLuint gShaderProgramSingleColor;
extern GLuint gShaderProgramFillScreenQuad;
extern struct Entity fillScreenQuadEntity;
struct FBO gFbo;

// skybox
extern GLuint gShaderProgramSkybox;

// prepare the entities for rendering
void prepareMasterRenderer(glm::mat4 projectionMatrix)
{
	// code
	prepareEntityRenderer(projectionMatrix);
	prepareEntityMapsRenderer(projectionMatrix);
	prepareSunRenderer(projectionMatrix); 
	prepareTerrainRenderer(projectionMatrix);
	prepareGeneralRenderer(projectionMatrix);
	prepareSingleColorRenderer(projectionMatrix);
	setUpFrameBuffer(&gFbo); // create FBO with color buffer
	prepareFillScreenQuadRenderer();
	prepareSkybox(projectionMatrix);
	preparePostProcessing();
}



// render entities
void renderScene(std::vector<struct Light> lights, struct Camera* camera)
{
	// draw scene as normal, but don't write the floor to the stencil buffer, we only care about the containers. 
	// We set its mask to 0x00 to not write to the stencil buffer.
	glStencilMask(0x00);
	glm::mat4 viewMatrix = createViewMatrix(*camera);
	// render entities
	// start entity shader program
	startProgram(gShaderProgramEntity);
	loadProjectionMatrix(gProjectionMatrix);
	loadViewMatrix(viewMatrix);
	loadLightProperties(lights, camera);
	loadViewerPosition(camera->position);
	if (entities.size() > 0)
	{
		// bind VAO
		glBindVertexArray(entities[0].texturedModel.rawModel.vaoID);
		renderEntity();
		// unbind VAO
		glBindVertexArray(0);
	}
	// stop shader program
	stopProgram(); // entity shader program ended
	

	// start entity maps shader program
	startProgram(gShaderProgramEntityMaps);
		loadProjectionMatrixEntityMaps(gProjectionMatrix);
		loadViewMatrixEntityMaps(viewMatrix);
		loadLightPropertiesEntityMaps(lights, camera);
		loadViewerPositionEntityMaps(camera->position);

		// render loaded models via assimp
		if (gLoadedModels.size() > 0)
		{
			glm::mat4 transformationMatrix = glm::mat4(1.0);
			// load uniforms
			struct EntityMap entityMap;
			entityMap.translate = glm::vec3(0.0, -0.5, -0.9);
			entityMap.rotateX = 10;
			entityMap.rotateY = 0;
			entityMap.rotateZ = 0;
			entityMap.scale = 0.1f;
			loadModelMatrixEntityMaps(getTransformationMatrixEntityMaps(transformationMatrix, entityMap));
			for (unsigned int i = 0; i < gLoadedModels.size(); i++)
			{
				gLoadedModels[i].draw(gShaderProgramEntityMaps);
			}
		}

		// render other entity mapped objects
		renderObjEntityMaps(lights, camera, gProjectionMatrix);

		// render player
		// bind VAO
		glBindVertexArray(player.texturedModel.rawModel.vaoID);
		loadUseFakeLightingEntityMaps(player.useFakeLighting);
		glm::mat4 transformationMatrix = glm::mat4(1.0);
		// load uniforms
		loadModelMatrixEntityMaps(getTransformationMatrixEntityMaps(transformationMatrix, player));
		loadMaterialPropertiesEntityMaps(player.material);
		// bind texture units
		bindTextureUnitsEntityMaps(player.texturedModel, player.material);
		// draw entity
		draw(player.texturedModel.rawModel.vertexCount, true);
		glBindVertexArray(0);
	// stop shader program
	stopProgram(); // entity maps shader program ended

	// render lights
	// start lights shader program
	startProgram(gShaderProgramSun);
	if (lights.size() > 0)
	{
		// bind VAO
		glBindVertexArray(lights[0].entity.texturedModel.rawModel.vaoID);
		loadProjectionMatrixSun(gProjectionMatrix);
		loadViewMatrixSun(viewMatrix);
		renderLights();
		// unbind VAO
		glBindVertexArray(0);
	}
	// stop shader program
	stopProgram(); // sun shader ended

	// render terrain
	startProgram(gShaderProgramTerrain);
	loadProjectionMatrixTerrain(gProjectionMatrix);
	loadViewMatrixTerrain(viewMatrix);
	loadLightPropertiesTerrain(lights, camera);
	loadViewerPositionTerrain(camera->position);
	renderTerrain();
	// stop terrain shader program
	stopProgram(); // terrain shader ended

	
	// render general entitieis
	// start entity shader program
	startProgram(gShaderProgramGeneral);
	if (gGeneralEntities.size() > 0)
	{
		loadProjectionMatrixGeneral(gProjectionMatrix);
		// bind VAO
		glBindVertexArray(gGeneralEntities[0].texturedModel.rawModel.vaoID);
		renderGeneral(camera);
		// unbind VAO
		glBindVertexArray(0);
	}
	// stop shader program
	stopProgram(); // general shader program ended
	

	// render entities with maps
	/*
	By using the GL_ALWAYS stencil testing function we make sure that each of the containers' fragments 
	update the stencil buffer with a stencil value of 1.
	*/
	glStencilFunc(GL_ALWAYS, 1, 0xFF); // all fragments should update the stencil buffer
	glStencilMask(0xFF); // enable writing to the stencil buffer
	// start entity shader program
	startProgram(gShaderProgramEntityMaps);
	if (entityMaps.size() > 0)
	{
		// bind VAO
		glBindVertexArray(entityMaps[0].texturedModel.rawModel.vaoID);
		loadProjectionMatrixEntityMaps(gProjectionMatrix);
		loadViewMatrixEntityMaps(viewMatrix);
		loadLightPropertiesEntityMaps(lights, camera);
		loadViewerPositionEntityMaps(camera->position);
		renderEntityMaps();
		// unbind VAO
		glBindVertexArray(0);
	}
	// stop shader program
	stopProgram();

	/*
	Now that the stencil buffer is updated with 1s where the containers were drawn 
	we're going to draw the upscaled containers, but this time disabling writes to the stencil buffer
	we're only drawing parts of the containers that are not equal to 1 thus only draw the part of the 
	containers that are outside the previously drawn containers.
	*/
	glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
	glStencilMask(0x00); // disable writing to the stencil buffer
	// Note that we also disable depth testing so the scaled up containers e.g. the borders do not get overwritten by the floor
	glDisable(GL_DEPTH_TEST);
	// start entity shader program
	startProgram(gShaderProgramSingleColor);
	if (entityMaps.size() > 1)
	{
		// bind VAO
		glBindVertexArray(entityMaps[0].texturedModel.rawModel.vaoID);
		loadProjectionMatrixSingleColor(gProjectionMatrix);
		loadViewMatrixSingleColor(viewMatrix);
		//loadLightPropertiesEntityMaps(lights, camera);
		//loadViewerPositionEntityMaps(camera->position);
		renderSingleColor(camera, entityMaps);
		// unbind VAO
		glBindVertexArray(0);
	}
	// stop shader program
	stopProgram(); // single color shader program ended

	glStencilMask(0xFF);
	glEnable(GL_DEPTH_TEST);
}

// clean up activities
void cleanUpMasterRenderer()
{
	cleanUpGeneral();
	cleanUpTerrain();
	cleanUpLights();
	cleanUpEntityMaps();
	cleanUpEntities();
}

void renderAll(std::vector<struct Light> lights, struct Camera* camera)
{
	//glViewport(0, 0, gWindowWidth, gWindowHeight);
	// first pass
	//bindFbo(gFbo.fboId);
	glClearColor(0.2f, 0.4f, 0.9f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	enableDepthTesting();
	
	renderScene(lights, camera);

	// render skybox starts
	glDepthFunc(GL_LEQUAL);  // change depth function so depth test passes when values are equal to depth buffer's content
	startProgram(gShaderProgramSkybox);
	loadProjectionMatrixSkybox(gProjectionMatrix);
	loadViewMatrixSkybox(createViewMatrix(*camera));
	renderSkybox();
	stopProgram();
	glDepthFunc(GL_LESS); // set depth function back to default
	// render skybox ends

	/*startProgram(gShaderProgramFillScreenQuad);
	//enableWireframeMode();
	renderFillScreenQuad(&gFbo);
	//disableWireframeMode();*/
	//doPostProcessing(&gFbo);
	
	
}