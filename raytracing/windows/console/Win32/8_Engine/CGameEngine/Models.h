#ifndef H_MODELS
#define H_MODELS

// headers
#include<iostream>
#include<stdio.h>
#include<Windows.h>
#include<vector>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include<string>
#include"GameUtils.h"
#include"Loader.h"
#include"stb_image.h"

using namespace std;

// models
struct MeshModel
{
	// mesh data
	std::vector<struct Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<struct Texture> textures;
	unsigned int VAO;
};

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoords;
	glm::vec3 tangent;
	glm::vec3 biTangent;
};

struct Texture
{
	unsigned int textureID;
	string type;
	string path;
	GLuint width;
	GLuint height;
	GLenum internalFormat;
	GLenum dataFormat;
};
struct RawModel
{
	GLuint vaoID;
	GLuint vertexCount;
};

struct TexturedModel
{
	struct RawModel rawModel;
	//std::vector<Mesh> objectMeshes; // for complex objects
	GLuint textureID;
	GLuint skyoxID;
};

// materials properties of an objects
struct Material
{
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	float shininess;
};

// materials properties of an objects
struct EntityMapMaterial
{
	GLuint diffuse;
	GLuint specular;
	GLuint backgroundTexture_sampler;
	GLuint rTexture_sampler;
	GLuint gTexture_sampler;
	GLuint bTexture_sampler;
	GLuint blendMap_sampler;
	float shininess;
};

// textured models having it's own translations, rotations and scaling factors
struct Entity
{
	struct TexturedModel texturedModel;
	glm::vec3 translate;
	float rotateX;
	float rotateY;
	float rotateZ;
	float scale; // uniform scaling
	struct Material material;
};

struct EntityMap
{
	struct TexturedModel texturedModel;
	glm::vec3 translate;
	float rotateX;
	float rotateY;
	float rotateZ;
	float scale; // uniform scaling
	bool isBordered; // is bordered entity
	bool useFakeLighting;
	struct EntityMapMaterial material;
};

// light source - as light is also one of the entity
/*
	0 - Directional light
	1 - Point light
	2 - Spot light
*/
struct Light
{
	struct Entity entity;
	glm::vec3 lightDirection;  //a direction from the light source
	glm::vec3 lightColor;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	GLuint lightType;
	// for point Light
	glm::vec3 lightPosition;
	float constant;
	float linear;
	float quadratic;

	// for spot light
	float cutoff;
	float outerCutoff;
};

struct FBO
{
	GLuint fboId;
	GLuint colorAttachment;
};

struct Sphere
{
	glm::vec3 center; // center of the sphere
	float radius; // radius of the sphere
};

// function prototype declarations
void loadCubeToVAO(struct RawModel* rawModel, bool isCubeMap);
void loadQuadToVAO(struct RawModel* rawModel, bool isFillScreenQuadRequested);
GLuint loadSkybox();
void generateTerrainModel(int gridX, int gridZ, struct RawModel* terrainModel);

#endif // !H_MODELS
