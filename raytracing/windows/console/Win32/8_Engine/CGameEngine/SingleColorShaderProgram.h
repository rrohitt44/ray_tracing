#ifndef H_SHADERPROGRAM_SINGLE_COLOR
#define H_SHADERPROGRAM_SINGLE_COLOR

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"

void getAllUniformLocationsSingleColor(); // get uniform locations

// uniforms
extern GLuint gShaderProgramSingleColor;

void loadSingleColorTextureSampler(GLuint value);
void loadModelMatrixSingleColor(glm::mat4 transformationMatrix);
void loadViewMatrixSingleColor(glm::mat4 transformationMatrix);
void loadProjectionMatrixSingleColor(glm::mat4 transformationMatrix);
void bindTexturesSingleColor(struct TexturedModel model);
#endif // !H_SHADERPROGRAM_SINGLE_COLOR
