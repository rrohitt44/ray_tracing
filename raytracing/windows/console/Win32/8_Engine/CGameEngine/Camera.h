#ifndef H_CAMERA
#define H_CAMERA

// headers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include"Logger.h"
#include"Player.h"

// camera
struct Camera
{
	glm::vec3 position;
	glm::vec3 target; // looking at
	glm::vec3 up;
	glm::vec3 cameraFront;
};

struct Camera_Compute
{
	glm::vec3 origin;
	glm::vec3 lower_left_corner;
	glm::vec3 horizontal;
	glm::vec3 vertical;
	glm::vec3 u, v, w;
};

extern glm::vec3 lookFromCC;
extern glm::vec3 lookAtCC;
extern glm::vec3 vupCC;
extern float vFovCC;
extern float aspectCC;

extern struct Camera_Compute cameraCompute;
extern struct Camera camera;
extern glm::vec3 gCameraPosition;
void initializeCamera();
void initializeCCCamera();
void updateCameraPosition(glm::vec3 position);
void updateComputeCamera(glm::vec3 lookFrom, glm::vec3 lookAt, glm::vec3 vup, float vFov, float aspect);
#endif // !H_CAMERA
