#ifndef H_SHADERPROGRAM_INVERSION
#define H_SHADERPROGRAM_INVERSION

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"

void getAllUniformLocationsInversion(); // get uniform locations
// uniforms
extern GLuint gShaderProgramInversion;

void loadTextureSamplerInversion(GLuint value);
void bindTexturesInversion(struct TexturedModel model);
#endif // !H_SHADERPROGRAM_INVERSION
