#ifndef H_TERRAIN_RENDERER
#define H_TERRAIN_RENDERER

// headers
#include<iostream>
#include<vector>
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"TerrainShaderProgram.h"
#include"Camera.h"
#include"Loader.h"
#include"OpenGLUtils.h"
#include"Mesh.h"

extern std::vector<EntityMap> terrain;
extern GLuint gShaderProgramTerrain;
void prepareTerrainRenderer(glm::mat4 projectionMatrix);
void processTerrain(struct EntityMap entity); // add entity to the entity list for rendering
void renderTerrain(); // render entities
void cleanUpTerrain(); // clean up the shader program object
#endif // !H_TERRAIN_RENDERER
