// Headers
#include<Windows.h>

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variable declarations
bool gbDone = false;
bool gbFullscreen = false;
DWORD gDwStyle;
HWND gHwnd;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine,
	int nCmdShow)
{
	//local variables
	WNDCLASSEX wndclass;
	TCHAR AppName[] = TEXT("Windows");
	HWND hwnd; //handle to window
	MSG msg;
	RECT rect; //for centering the window

	//code
	//initialize window class
	wndclass.cbSize = sizeof(WNDCLASSEX); //size in bytes of this structure
	wndclass.style = CS_HREDRAW | CS_VREDRAW; //class styles
	wndclass.cbClsExtra = 0; //number of extra bytes to allocate following the window-class structure
	wndclass.cbWndExtra = 0; //number of extra bytes to allocate following the window instance
	wndclass.lpszMenuName = NULL; //resource name of class menu
	wndclass.lpszClassName = AppName; //class name
	wndclass.lpfnWndProc = WndProc; //pointer to window procedure
	wndclass.hInstance = hInstance; //handle to the instance that contains the window procedure
	wndclass.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH); //handle to the class background brush
	wndclass.hIcon = LoadIcon(hInstance, TEXT("IDI_ICON1")); //handle to the class icon
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION); //handle to the small icon that is associated with the window class
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); //handle to the cursor class

	//register the window class
	RegisterClassEx(&wndclass);

	//get window parameters for centering
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);
	int width = (rect.right - rect.left) / 2 - WIN_WIDTH / 2;
	int height = (rect.bottom - rect.top) / 2 - WIN_HEIGHT / 2;

	//create the window in memory
	hwnd = CreateWindow(
		AppName, //class name
		TEXT("Window with fullscreen"), //window name
		WS_OVERLAPPEDWINDOW, //style
		width, //x
		height, //y
		WIN_WIDTH, //width
		WIN_HEIGHT, //height
		NULL, //wnd parent
		NULL, //menu
		hInstance, //hInstance
		NULL //param
	);

	//error checking
	if (hwnd == NULL)
	{
		MessageBox(NULL, //hWnd
			TEXT("Window Not Created"), //text
			TEXT("Error..."), //caption
			0 //utype
		);

		exit(0);
	}

	gHwnd = hwnd;

	ShowWindow(hwnd, nCmdShow); //show window on desktop
	UpdateWindow(hwnd); //update the client area

	//message loop
	/*while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}*/
	while (gbDone == false)
	{
		//check if message is there
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			//if peeked message is WM_QUIT
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else //else translate and dispatch the message
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//rendering starts here
		}
	}

	return ((int)msg.wParam);
}


//Window Procedure
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local variables
	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	HBRUSH hBrush;

	//function prototype declarations
	void ToggleFullscreen(void);

	//code
	//handle messages
	switch (iMsg)
	{ 
		//change window color
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rect);
		hBrush = CreateSolidBrush(RGB(0, 0, 255));
		FillRect(hdc, &rect, hBrush);
		EndPaint(hwnd, &ps);
		break;

		//when closing window
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE: //when escape is pressed then close the window
			DestroyWindow(hwnd);
			break;

		case 0x46: //f/F key press
			ToggleFullscreen();
			break;
		default:
			break;
		}
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

/* ToggleFullscreen:
	toggles the window: from fullscreen to normal and vice versa
*/
void ToggleFullscreen(void)
{
	//local variables
	HMONITOR hMonitor;

	//code
	//if window is not in fullscreen
	if (gbFullscreen == false)
	{
		//get current window style
		gDwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		//check if it includes the overlapped window
		if (gDwStyle & WS_OVERLAPPEDWINDOW)
		{
			//evaluate current window placement
			bool bWindowPlacement = GetWindowPlacement(gHwnd, &wpPrev);

			//take the information of the window which you want to make fullscreen
			hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
			MONITORINFO mi;
			mi.cbSize = sizeof(MONITORINFO);
			bool bMonitorInfo = GetMonitorInfo(hMonitor, &mi);

			if (bWindowPlacement && bMonitorInfo)
			{
				//set window style accordingly
				SetWindowLong(gHwnd, GWL_STYLE, gDwStyle & ~WS_OVERLAPPEDWINDOW);

				//set window position accordingly
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			//hide the cursor
			ShowCursor(FALSE);
			gbFullscreen = true;
		}
	}
	else //if already fullscreen
	{
		//code to restore the fullscreen
		SetWindowLong(gHwnd, GWL_STYLE, gDwStyle & WS_OVERLAPPEDWINDOW);
		//set window placement back to previous
		SetWindowPlacement(gHwnd, &wpPrev);
		//set window position
		SetWindowPos(gHwnd, HWND_TOP, 0,0,0,0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		//show cursor
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}