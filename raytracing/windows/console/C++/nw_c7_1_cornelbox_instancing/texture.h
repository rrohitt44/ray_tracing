#ifndef TEXTUREH
#define TEXTUREH

#include "perlin.h"

/*
 * function that makes the colors on a surface procedural
 * This can be systhesis code or image look up or both
 * */
class texture
{
public:
	virtual vec3 value(float u, float v, const vec3& p) const = 0;
};

class constant_texture : public texture
{
public:
	//varialbes
	vec3 color;

	//constructors
	constant_texture(){}
	constant_texture(vec3 c): color(c){}

	//methods
	virtual vec3 value(float u, float v, const vec3& p) const
	{
		return color;
	}
};

/*
 * We can create a checker texture by noting that the sign of a sine and cosine just alternates
 * in a regular way and if we multiply trig functions in all three dimensions, the sign of that
 * product forms a 3D checker pattern
 * */
class checker_texture : public texture
{
//scope
public:
	//variables
	texture *odd;
	texture *even;

	//constructor
	checker_texture(){}
	checker_texture(texture *t0, texture *t1) : odd(t0), even(t1){}

	//methods
	virtual vec3 value(float u, float v, const vec3& p) const
	{
		float sines = sin(10 * p.x()) * sin(10 * p.y()) * sin(10 * p.z());

		if(sines < 0)
		{
			return odd->value(u, v, p);
		}else
		{
			even->value(u, v, p);
		}
	}
};

/*
 * takes the floats between 0 and 1 and creates gray colors
 * */
class noise_texture : public texture
{
public:
	//variables
	perlin noise;
	float scale;
	//constructors
	noise_texture(){}
	noise_texture(float sc) : scale(sc){}
	//override method
	virtual vec3 value(float u, float v, const vec3& p) const
	{
		//return vec3(1,1,1) * noise.noise(scale * p);
		//return vec3(1,1,1) * noise.turb(p);
		//return vec3(1,1,1) * 0.5 * (1+ noise.turb(scale * p));
		return vec3(1,1,1)* 0.5 * (1 + sin(scale*p.z() + 5*noise.turb(scale*p)));
	}
};

/*
 * image_texture: texture class that holds the image
 * stb_image reads in an image into a big array of unsigned char
 * There are just packed RGBs that range from 0 to 255
 * */
class image_texture : public texture
{
public:
	//variables
	unsigned char *data;
	int nx, ny;

	//constructors
	image_texture() {}
	image_texture(unsigned char *pixels, int A, int B) : data(pixels),  nx(A), ny(B){}

	//methods
	virtual vec3 value(float u, float v, const vec3& p) const;
};

vec3 image_texture::value(float u, float v, const vec3& p) const
{
	int i = (u) * nx;
	int j = (1-v) * ny - 0.001;

	if(i<0)
	{
		i = 0;
	}
	if(j<0)
	{
		j = 0;
	}

	if(i > nx-1)
	{
		i = nx - 1;
	}
	if(j > ny-1)
	{
		j = ny - 1;
	}

	float r = int(data[3*i + 3*nx*j + 0])/255.0;
	float g = int(data[3*i + 3*nx*j + 1])/255.0;
	float b = int(data[3*i + 3*nx*j + 2])/255.0;

	return vec3(r, g, b);
}
#endif
