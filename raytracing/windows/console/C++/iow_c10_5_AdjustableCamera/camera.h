#ifndef CAMERAH
#define CAMERAH

#include "ray.h"

class camera
{
public:
	vec3 lower_left_corner;
	vec3 origin;
	vec3 horizontal;
	vec3 verticle;
	camera(
			vec3 lookfrom, //where the camera is
			vec3 lookat, //where camera is looking at
			vec3 vup, //viewing up vector
			float vfov, //top to bottom in degrees - try changing this and see the effect
			float aspect //height to in distance(-z) ratio
			)
	{
		vec3 u, v, w;
		float theta = vfov * M_PI / 180; //converting vfov from degrees to radian
		float half_height = tan(theta/2);
		float half_width = aspect * half_height;
		origin = lookfrom;
		w = unit_vector(lookfrom - lookat);
		u = unit_vector(cross(vup , w));
		v = cross(w, u);
		lower_left_corner = vec3(-half_width, -half_height, -1.0);
		lower_left_corner = origin - half_width*u - half_height*v -w;
		horizontal = 2 * half_width*u;
		verticle = 2 * half_height*v;
	}

	ray get_ray(float u, float v)
	{
		return ray(origin, lower_left_corner+ u*horizontal + v*verticle - origin);
	}
};
#endif
