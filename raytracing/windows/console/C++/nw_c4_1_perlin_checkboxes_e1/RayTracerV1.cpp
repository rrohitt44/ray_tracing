#include<iostream>
#include "sphere.h"
#include "moving_sphere.h"
#include "hitable_list.h"
#include "float.h"
#include "camera.h"
#include "material.h"
#include "util.h"

//global variables declaration
FILE *gpImageFile = NULL;

//global functions
vec3 color(ray& r, hitable *world, int depth)
{
	//function prototype declaration
	vec3 random_in_unit_sphere(void);

	//local variables
	hit_record rec;

	//code
	//t_min = 0.001 because some of the reflected rays hit the object they are reflecting off
	//of not at exactly t=0, but instead at t=-0.0000001 or t=0.0000001 or whatever floating point
	//approximations the sphere intersectors gives us; so we need to ignore hits very near to zero;
	//this gets rids of the shadow acne problem
	if(world->hit(r, 0.001, FLT_MAX, rec))
	{
		ray scattered;
		vec3 attenuated;
		if(depth<50 && rec.mat_ptr->scatter(r, rec, attenuated, scattered))
		{
			return attenuated*color(scattered, world, depth+1);
		}else
		{
			return vec3(0,0,0);
		}

	}else
	{
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0f);
		return (1.0f - t) * vec3(1.0f, 1.0f, 1.0f) + t*vec3(0.5f, 0.7f, 1.0f);
	}
}

//generates lots of random spheres
hitable* random_scene()
{
	int n = 500;
	hitable **list = new hitable*[n+1];

	//base sphere
	texture *checker = new checker_texture(
								new constant_texture(vec3(0.2,0.3,0.1)), //t0
								new constant_texture(vec3(0.9,0.9,0.9)) //t1
								);
	list[0] = new sphere(vec3(0, -1000, 0), //center
			1000, //radius
			new lambertian(checker) //material
			);

	int i=1;

	for(int a=-11; a<11; a++)
	{
		for(int b=-11; b<11; b++)
		{
			float choose_mat = fRand();
			vec3 center(a+0.9*fRand(), 0.2, b+0.9*fRand());

			//set material
			if((center-vec3(4, 0.2, 0)).length() > 0.9)
			{
				//diffuse
				if(choose_mat < 0.8)
				{
					list[i++] = new moving_sphere(center, //center0
							center+vec3(0, 0.5*fRand(),0), //center1
							0.0, //time0
							1.0, //time1
							0.2, //radius
							new lambertian(new constant_texture(vec3(fRand()*fRand(), fRand()*fRand(), fRand()*fRand()))));
				}else if(choose_mat < 0.95) //metal
				{
					list[i++] = new sphere(center,
								0.2, //radius
								new metal(vec3(0.5 * (1+ fRand()), 0.5 * (1+ fRand()), 0.5 * (1+ fRand())),
											0.5*fRand() //fuzz
					));
				}else //glass
				{
					list[i++] = new sphere(center, 0.2, new dielectric(1.5));
				}
			}
		}
	}


	//three big spheres
	list[i++] = new sphere(
			vec3(0, 1, 0), //center
			1.0, //radius
			new dielectric(1.5) //material
			);
	list[i++] = new sphere(
			vec3(-4, 1, 0), //center
			1.0, //radius
			new lambertian(new constant_texture(vec3(0.4, 0.2, 0.1))) //material
			);
	list[i++] = new sphere(
			vec3(4, 1, 0), //center
			1.0, //radius
			new metal(vec3(0.7, 0.6, 0.5), 0.0) //material
			);

	return new hitable_list(list, i);
}

//two spheres scene
hitable *two_spheres()
{
	texture *checker = new checker_texture(
									new constant_texture(vec3(0.2,0.3,0.1)), //t0
									new constant_texture(vec3(0.9,0.9,0.9)) //t1
									);

	int n=50;
	hitable **list = new hitable*[n+1];
	list[0] = new sphere(
						vec3(0, -10, 0), //center
						10, //radius
						new lambertian(checker) //texture
					);

	list[1] = new sphere(
							vec3(0, 10, 0), //center
							10, //radius
							new lambertian(checker) //texture
						);

	new hitable_list(list, 2);
}

hitable *two_perlin_spheres()
{
	texture *pertext = new noise_texture();
	hitable **list = new hitable*[2];

	list[0] = new sphere(
							vec3(0, -1000, 0), //center
							1000, //radius
							new lambertian(pertext) //texture
						);

		list[1] = new sphere(
								vec3(0, 2, 0), //center
								2, //radius
								new lambertian(pertext) //texture
							);

		new hitable_list(list, 2);
}
int main()
{
	//function prototype declaration
	hitable* random_scene();
	hitable* two_spheres();
	hitable* two_perlin_spheres();

	//local variable declaration
int nx = 1220; //columns
int ny = 800; //rows
int ns = 10; //sample points

//code
//open file
gpImageFile = fopen("finalimage.ppm", "w");

//std::cout<< "P3\n" << nx <<" " << ny <<"\n255\n";
fprintf(gpImageFile, "P3\n%d %d\n255\n",nx, ny);

float R = cos(M_PI/4);
printf("hi main 1");

hitable *world = two_perlin_spheres();

vec3 lookfrom(13,2,3);
vec3 lookat(0,0,0);
float dist_to_focus = 10.0;
float aperture = 0.0;

camera cam(
				lookfrom,  //look from
				lookat, //look at
				vec3(0, 1, 0), //vertical up
				20, //vfov
				float(nx)/float(ny), //aspect ratio
				aperture,
				dist_to_focus,
				0.0, //time0
				1.0 //time1
				);

for(int j=ny-1; j>=0; j--)
{
	for(int i=0; i<nx; i++)
	{
		vec3 col(0,0,0); //initial color
		for(int s=0; s<ns; s++)
		{
			float u = float(i + fRand()) / float(nx);
			float v = float(j + fRand()) / float(ny);
			ray r = cam.get_ray(u, v);
			col += color(r, world, 0);
		}
		col /= float(ns);

		//making the image gamma 2 corrected : means raising the color to 1/gamma i.e. 1/2
		col = vec3(sqrt(col[0]),sqrt(col[1]), sqrt(col[2])); //final color
		int ir = int(255.99 * col[0]);
		int ig = int(255.99 * col[1]);
		int ib = int(255.99 * col[2]);
		//std::cout<< ir << " " << ig << " " << ib <<"\n";
		fprintf(gpImageFile, "%d %d %d\n",ir, ig, ib);
	}
}

//close the file
fclose(gpImageFile);
}

