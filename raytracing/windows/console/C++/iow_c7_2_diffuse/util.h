#ifndef UTILH
#define UTILH

#include "stdlib.h"

double fRand()
{
	double fMin=0.0, fMax=1.0;
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

#endif
