#ifndef HITABLEH
#define HITABLEH

#include "ray.h"
struct hit_record
{
	float t;
	vec3 p;
	vec3 normal;
};

class hitable
{
public:
	//hit only counts if t_min<t<t_max
	virtual bool hit(ray& r, //ray which is going to hit the object
			float t_min,//hit minumum interval
			float t_max,//hit maximum interval
			hit_record& rec) const = 0;
};

#endif
