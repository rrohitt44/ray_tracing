#ifndef MATERIALH
#define MATERIALH


#include "hitable.h"
#include "util.h"

class material
{
public:
	virtual bool scatter(ray& r_in,
			const hit_record& rec, //to avoid bunch of arguments
			vec3& attenuation, //how much ray should be attenuated
			ray& scattered //produced a scattered ray or absorbed incident ray
			) const=0;
};

/*
 * For lambertian (diffuse) case we already have, it can either scatter always and attenuate by its
 * reflectance R, or it can scatter with no attenuation but absorb the fraction  1-R of rays
 * or it could be mixture of those strategies
 * */
class lambertian : public material
{
public:
	vec3 albedo;
	lambertian(const vec3& a) : albedo(a){}

	virtual bool scatter(ray& r_in,
				const hit_record& rec, //to avoid bunch of arguments
				vec3& attenuation, //how much ray should be attenuated
				ray& scattered //produced a scattered ray or absorbed incident ray
				) const
	{
		//function prototype declaration
		vec3 random_in_unit_sphere(void);

		//code
		//pick a random point from the unit radius sphere that is tangent to the hitpoint
		vec3 target = rec.p + rec.normal + random_in_unit_sphere();

		//ray from hitpoint to random point
		scattered = ray(rec.p, target-rec.p);

		attenuation = albedo;
		return true;
	}
};

/*
 * v - incident ray
 * n = normal
 * */
vec3 reflect(const vec3& v, const vec3& n)
{
	return v - 2*dot(v, n)*n;
}


/*
 * For a smooth metal, a ray won't be randomly scattered
 * */
class metal : public material
{
public:
	vec3 albedo;
	float fuzz;
	metal(const vec3& a, float f) : albedo(a)
	{
		if(f < 1)
		{
			fuzz = f;
		}else
		{
			fuzz = 1;
		}
	}
	virtual bool scatter(ray& r_in,
					const hit_record& rec, //to avoid bunch of arguments
					vec3& attenuation, //how much ray should be attenuated
					ray& scattered //produced a scattered ray or absorbed incident ray
					) const
		{
			vec3 r_in_dir = r_in.direction();
			vec3 r_in_unit_dir = unit_vector(r_in_dir);
			vec3 reflected = reflect(r_in_dir, rec.normal);
			scattered = ray(rec.p, reflected + fuzz * random_in_unit_sphere());
			attenuation= albedo;
			return (dot(scattered.direction(), rec.normal) > 0);
		}
};
#endif
