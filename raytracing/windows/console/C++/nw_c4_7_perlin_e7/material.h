#ifndef MATERIALH
#define MATERIALH


#include "hitable.h"
#include "util.h"
#include"texture.h"
class material
{
public:
	virtual bool scatter(ray& r_in,
			const hit_record& rec, //to avoid bunch of arguments
			vec3& attenuation, //how much ray should be attenuated
			ray& scattered //produced a scattered ray or absorbed incident ray
			) const=0;
};

/*
 * For lambertian (diffuse) case we already have, it can either scatter always and attenuate by its
 * reflectance R, or it can scatter with no attenuation but absorb the fraction  1-R of rays
 * or it could be mixture of those strategies
 *
 *
 * New: now we make textured materials by replacing the vec3 color with a texture pointer
 * */
class lambertian : public material
{
public:
	texture *albedo;
	lambertian(texture *a) : albedo(a){}

	virtual bool scatter(ray& r_in,
				const hit_record& rec, //to avoid bunch of arguments
				vec3& attenuation, //how much ray should be attenuated
				ray& scattered //produced a scattered ray or absorbed incident ray
				) const
	{
		//function prototype declaration
		vec3 random_in_unit_sphere(void);

		//code
		//pick a random point from the unit radius sphere that is tangent to the hitpoint
		vec3 target = rec.p + rec.normal + random_in_unit_sphere();

		//ray from hitpoint to random point at the time of incident ray
		scattered = ray(rec.p, target-rec.p, r_in.time());

		attenuation = albedo->value(0, 0, rec.p);
		return true;
	}
};

/*
 * v - incident ray
 * n = normal
 * */
vec3 reflect(const vec3& v, const vec3& n)
{
	return v - 2*dot(v, n)*n;
}


/*
 * For a smooth metal, a ray won't be randomly scattered
 * */
class metal : public material
{
public:
	vec3 albedo;
	float fuzz;
	metal(const vec3& a, float f) : albedo(a)
	{
		if(f < 1)
		{
			fuzz = f;
		}else
		{
			fuzz = 1;
		}
	}
	virtual bool scatter(ray& r_in,
					const hit_record& rec, //to avoid bunch of arguments
					vec3& attenuation, //how much ray should be attenuated
					ray& scattered //produced a scattered ray or absorbed incident ray
					) const
		{
			vec3 r_in_dir = r_in.direction();
			vec3 r_in_unit_dir = unit_vector(r_in_dir);
			vec3 reflected = reflect(r_in_dir, rec.normal);
			scattered = ray(rec.p, reflected + fuzz * random_in_unit_sphere());
			attenuation= albedo;
			return (dot(scattered.direction(), rec.normal) > 0);
		}
};

/*
 * refraction is described by snell's law -
 * n sin(theta) = n' sin(theta'); where n and n' are refractive indices
 * air = 1, glass = 1.3-1.7, diamond=2.4
 * */
bool refract(const vec3& v, const vec3& n, float ni_over_nt, vec3& refracted)
{
	vec3 uv = unit_vector(v);
	float dt = dot(uv, n);
	float discriminant = 1.0 - ni_over_nt * ni_over_nt * (1 - dt*dt);
	if(discriminant > 0)
	{
		refracted = ni_over_nt * (uv - n*dt) - n*sqrt(discriminant);
		return true;
	}else
	{
		return false;
	}
}

/*
 * Dielectric material always refracts when possible;
 *
 * An interesting and easy trick with a dielectric sphere is to note that if you use a negative radius,
 * the geometry is unaffected but the surface normal points inwards, so it can be used as a bubble
 * to make a hollow glass sphere
 * */
class dielectric : public material
{
public:
	float ref_idx;
	dielectric(float refractiveIndex) : ref_idx(refractiveIndex){}

	virtual bool scatter(ray& r_in,
					const hit_record& rec, //to avoid bunch of arguments
					vec3& attenuation, //how much ray should be attenuated
					ray& scattered //produced a scattered ray or absorbed incident ray
					) const
		{
		//function prototype declaration
		float schlick(float cosine, float ref_idx);

		//code
			vec3 outward_normal;
			vec3 reflected = reflect(r_in.direction(), rec.normal);
			float ni_over_nt;

			//glass surface absorbs nothing; so attenuation is always 1
			//this also kills the blue channel which is the type of color bug
			//to see difference try with vec3(1.0, 1.0, 1.0)
			attenuation = vec3(1.0, 1.0, 1.0);
			vec3 refracted;

			float reflect_prob;
			float cosine;
			if(dot(r_in.direction(), rec.normal) > 0)
			{
				outward_normal = -rec.normal;
				ni_over_nt = ref_idx;
				cosine = ref_idx * dot(r_in.direction(), rec.normal) / r_in.direction().length();
			}else
			{
				outward_normal = rec.normal;
				ni_over_nt = 1.0 / ref_idx;
				cosine = -dot(r_in.direction(), rec.normal) / r_in.direction().length();
			}


			//when there is a reflection ray the function returns false, so there
			//are no reflections
			if(refract(r_in.direction(), outward_normal, ni_over_nt, refracted))
			{
				reflect_prob = schlick(cosine, ref_idx);
				//scattered = ray(rec.p, refracted);
			}else
			{
				reflect_prob = 1.0;
				//scattered = ray(rec.p, reflected);
			}

			if(fRand() < reflect_prob)
			{
				scattered = ray(rec.p, reflected);
			}else
			{
				scattered = ray(rec.p, refracted);
			}
			return true;
		}
};

/*
 * Real glass has reflectivity that varies with angle;
 * look at a window at a steep angle and it will becomes a mirror;
 * to calculate this we use a simple polynomial approximation by Christophe Schlick
 * This yeilds a full glass material
 */
float schlick(float cosine, float ref_idx)
{
	float r0 = (1-ref_idx) / (1+ref_idx);
	r0 = r0 * r0;
	return r0 + (1-r0)*pow((1-cosine), 5);
}
#endif
