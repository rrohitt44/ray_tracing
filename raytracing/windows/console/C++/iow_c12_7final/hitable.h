#ifndef HITABLEH
#define HITABLEH

#include "ray.h"

//material will tell us how rays interact with the surface
class material;

struct hit_record
{
	float t;
	vec3 p;
	vec3 normal;

	//when a ray hits the surface e.g. sphere, this material pointer will be set to point at the
	//material pointer the sphere was given when it was set up in main() when we start up
	material *mat_ptr;
};

class hitable
{
public:
	//hit only counts if t_min<t<t_max
	virtual bool hit(ray& r, //ray which is going to hit the object
			float t_min,//hit minumum interval
			float t_max,//hit maximum interval
			hit_record& rec) const = 0;
};

#endif
