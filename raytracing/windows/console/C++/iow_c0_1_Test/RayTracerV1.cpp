#include<iostream>
#include"ray.h"
//global variables declaration
FILE *gpImageFile = NULL;

vec3 color(ray& r)
{
	vec3 unit_direction = unit_vector(r.direction());
	float t = 0.5 * (unit_direction.y() + 1.0);
	return (1.0 - t) * vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
}

int main()
{
int nx = 200; //columns
int ny = 1; //rows
//open file
gpImageFile = fopen("finalimage.ppm", "w");

//std::cout<< "P3\n" << nx <<" " << ny <<"\n255\n";
fprintf(gpImageFile, "P3\n%d %d\n255\n",nx, ny);

vec3 lower_left_corner(-2.0, -1.0, -1.0);
vec3 horizontal(4.0, 0.0, 0.0);
vec3 vertical(0.0, 2.0, 0.0);
vec3 origin(0.0, 0.0, 0.0);
for(int j=ny-1; j>=0; j--)
{
	for(int i=0; i<nx; i++)
	{

		float u = float(i)/float(nx);
		float v = float(j)/float(ny);
		//float b =0.2;
		ray r(origin, lower_left_corner + u*horizontal + v*vertical);
		vec3 col = color(r);
		int ir = int(255.99 * col[0]);
		int ig = int(255.99 * col[1]);
		int ib = int(255.99 * col[2]);
		//std::cout<< ir << " " << ig << " " << ib <<"\n";
		fprintf(gpImageFile, "%d %d %d\n",ir, ig, ib);
	}
}

//close the file
fclose(gpImageFile);
}
